﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms;
using System.Globalization;

namespace Exedy_Login
{
    public partial class Principal : MaterialSkin.Controls.MaterialForm
    {
        public Principal(int idusuario)
        {
            InitializeComponent();
            MaterialSkin.MaterialSkinManager skinManager = MaterialSkin.MaterialSkinManager.Instance;
            skinManager.AddFormToManage(this);
            skinManager.Theme = MaterialSkin.MaterialSkinManager.Themes.LIGHT;
            skinManager.ColorScheme = new MaterialSkin.ColorScheme(MaterialSkin.Primary.Blue400, MaterialSkin.Primary.BlueGrey900, MaterialSkin.Primary.BlueGrey500, MaterialSkin.Accent.Green700, MaterialSkin.TextShade.WHITE);
            this.id_usuario = idusuario;
        }
        int id_usuario;    
        BD conexion = new BD();
        UsuarioDAL usuario = new UsuarioDAL();
        BackgroundWorker tarea2 = new BackgroundWorker();
        int contador_ng;
        int contador_ok;
        string QR_de_la_busqueda_en_ultima_estacion;
        int bandera;
        Boolean saliendo = false;
        string Inspector;
        int turno;
        int hora_turno;
        int hora_minuto;
        bool GenReporte = true;
        //int minutos;
        int turno_empleado;
        int salida;
        string QR_para_comparar;
        //String files;
        String path_ultimo;
        string respuestapto;

        // Creates or loads an INI file in the same directory as your executable
        // named EXE.ini (where EXE is the name of your executable)
        //var MyIni = new IniFile();

        // Or specify a specific name in the current dir
        //var MyIni = new IniFile("Settings.ini");

        // Or specify a specific name in a specific dir
        IniFile MyIni = new IniFile(@"C:\sistema\exedi.ini");

        public static SqlConnection cn = new SqlConnection("Data Source=192.168.0.100;Initial Catalog=EXEDY;Persist Security Info=True;User ID=admin;Password=megadeth");

        public int counter, SHUTER, CiclosShuter, RetardoCam;
        public string puerto;
        public int velpto;


        public string Limpiarcad(string cadena)
        {
            return cadena.Trim(new char[] { ' ', '"', ':' }); 
        }

        private void PuertosDisponibles()
        {
            foreach (string puertoDis in System.IO.Ports.SerialPort.GetPortNames())
            {
                CmbPuertos.Items.Add(puertoDis);
            }
            SerialPort1.PortName = puerto;
            SerialPort1.BaudRate = 115200;
            SerialPort1.DataBits = 8;
            SerialPort1.Parity = Parity.Even;
            SerialPort1.StopBits = StopBits.One;
            SerialPort1.Handshake = Handshake.None;
            SerialPort1.RtsEnable = true;
            SerialPort1.DtrEnable = true;
            CmbPuertos.Enabled = false;
            try
            {
                SerialPort1.Open();
            }
            catch (Exception )
            {
                AutoClosingMessageBox.Show("Selecciones otro puerto", "Puerto no disponible",2000);
                CmbPuertos.Enabled = true;
            }
        }

        private void SerialPort1_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            string datorx = SerialPort1.ReadExisting();
            respuestapto = datorx.ToString();
        }
        private void CmbPuertos_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            SerialPort1.PortName = CmbPuertos.Text;
            SerialPort1.BaudRate = 115200;
            SerialPort1.DataBits = 8;
            SerialPort1.Parity = Parity.Even;
            SerialPort1.StopBits = StopBits.One;
            SerialPort1.Handshake = Handshake.None;
            SerialPort1.RtsEnable = true;
            SerialPort1.DtrEnable = true;

            CmbPuertos.Enabled = false;
            try
            {
                SerialPort1.Open();
            }
            catch (Exception)
            {
                AutoClosingMessageBox.Show("Selecciones otro puerto", "Puerto no disponible",2000);
                CmbPuertos.Enabled = true;
            }
        }
        public String Wr(/*object sender, EventArgs e,*/String cadena)
        {
            try
            {
                if (SerialPort1.IsOpen== false)
                { SerialPort1.Open(); }
                SerialPort1.WriteLine(cadena + Char.ConvertFromUtf32(13));
                Thread.Sleep(30);
                return SerialPort1.ReadExisting();
            }
            catch (Exception )
            {
                AutoClosingMessageBox.Show("No se puedo enviar la información", "Error", 2000);
                return "";
            }
        }
    

        private void Principal_Load(object sender, EventArgs e)
        {
            conexion.limpiacachesql();
            try
            {
                if (MyIni.KeyExists("CountDown", "Constantes"))
                {
                    counter = Convert.ToInt32(MyIni.Read("CountDown", "Constantes"));
                }
                else
                {
                    MyIni.Write("CountDown", "10", "Constantes");
                    counter = 10;
                }
                txtcountdown.Text = Convert.ToString(counter);
                if (MyIni.KeyExists("CiclosShuter", "Constantes"))
                {
                    CiclosShuter = Convert.ToInt32(MyIni.Read("CiclosShuter", "Constantes"));
                }
                else
                {
                    MyIni.Write("CiclosShuter", "1", "Constantes");
                    CiclosShuter = 1;
                }
                if (MyIni.KeyExists("Puerto", "Constantes"))
                {
                    puerto = MyIni.Read("Puerto", "Constantes");
                }
                else
                {
                    MyIni.Write("Puerto", "COM4", "Constantes");
                    puerto = "COM4";
                }
                if (MyIni.KeyExists("SHUTER", "Constantes"))
                {
                    SHUTER = Convert.ToInt32(MyIni.Read("SHUTER", "Constantes"));
                }
                else
                {
                    MyIni.Write("SHUTER", "0", "Constantes");
                    SHUTER = 0;
                }
                if (MyIni.KeyExists("RetardoCam", "Constantes"))
                {
                    RetardoCam = Convert.ToInt32(MyIni.Read("RetardoCam", "Constantes"));
                }
                else
                {
                    MyIni.Write("RetardoCam", "50", "Constantes");
                    RetardoCam = 50;
                }
                PuertosDisponibles();
                Inspector = conexion.Usuario(Turno_ahora());
                Console.ReadLine();
                if (MyIni.KeyExists("contador_ok", "Variables"))
                {
                    contador_ok = Convert.ToInt32(MyIni.Read("contador_ok", "Variables"));
                }
                else
                {
                    MyIni.Write("contador_ok", "0", "Variables");
                    contador_ok = 0;
                }
                if (MyIni.KeyExists("contador_ng", "Variables"))
                {
                    contador_ng = Convert.ToInt32(MyIni.Read("contador_ng", "Variables"));
                }
                else
                {
                    MyIni.Write("contador_ng", "0", "Variables");
                    contador_ng = 0;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
            Txtok.Text = contador_ok.ToString();
            Txtng.Text = contador_ng.ToString();
            TextBox16.Text = "";
            Wr("wr dm11245.u " + Convert.ToString(SHUTER));//le indicamos al PLC si va a usar el Shuter o no, ese valor sale del exedy.ini
            Wr("wr dm11246.u " + Convert.ToString(CiclosShuter));//le indicamos al PLC si va a contar las piezas en el Shuter o no, ese valor sale del exedy.ini
            Wr("wr dm11242.u " + Convert.ToString(DateTime.Now.Year).Substring(2)); //le mandamos el año al plc para validar el qr, por si perdio la fecha el plc
            Wr("WR DM11240.U 000");// avisa al plc que termino el CountDown y que el prejuicio es OK 111 o ng 222
            Wr("WR DM11260.U 000");// El PLC nos avisa si precionaron el botton OK 111 o el boton ng 222
            Wr("WR DM11255.U "+Convert.ToString(RetardoCam));// El PLC retardo lectura camaras  50=500ms
            Busca_ingresa_bd();
            this.Refresh();
            Application.DoEvents();
            path_ultimo = Busqueda_de_QR(6); //20180217 OMAR:
            try
            {
                if (path_ultimo.EndsWith("csv"))
                {
                    File.Delete(path_ultimo);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
            path_ultimo = Busqueda_de_QR(7); //20180217 OMAR:
            try
            {
                if (path_ultimo.EndsWith("csv"))
                {
                    File.Delete(path_ultimo);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
            while ( saliendo == false)
            {
                Application.DoEvents();
                this.Refresh();
                TraerultimoQR(6);
                salida = Turno_salida();
                if (salida == 1 && GenReporte)
                {
                    try
                    {
                        Export_DataTable_PDF.ExportarPDF pdf = new Export_DataTable_PDF.ExportarPDF(Inspector);
                        pdf.ShowDialog();
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    //this.Hide();
                    MyIni.Write("contador_ok", "0", "Variables");
                    contador_ok = 0;
                    MyIni.Write("contador_ng", "0", "Variables");
                    contador_ng = 0;
                    Txtok.Text = "0";
                    Txtng.Text = "0";
                    Txtok.Refresh();
                    Txtng.Refresh();
                    this.Refresh();
                    GenReporte = false;
                    conexion.limpiacachesql();
                }
                Console.WriteLine("Termino la 6");
                GC.Collect();
            }// while ( saliendo == false)
        }//private void principal_Load(object sender, EventArgs e)

        String searchFolder;
        string lectura = "";
        string lectura22 = "";
        string lectura23 = "";
        string qr = "";
        double medicion = 0;
        double medicion22 = 0;
        double medicion23 = 0;
        string estatus = "";
        string visual = "";
        int Esta1 = 0;
        int Esta2 = 0;
        int Esta3 = 0;
        int Esta4 = 0;
        int Esta5g = 0;
        int Esta5v = 0;
        //string esta5 ;
        string[] consulta_pantalla_principal_del_ultimo_ingreso;
        //string[] consulta_pantalla_principal;


        public string Busqueda_de_QR(int num)
        {
            switch (num)
            {
                case 1:
                    searchFolder = @"C:\inetpub\FTP\Estacion1\";
                    break;
                case 2:
                    searchFolder = @"C:\inetpub\FTP\Estacion2\";
                    break;
                case 3:
                    searchFolder = @"C:\inetpub\FTP\Estacion3\";
                    break;
                case 4:
                    searchFolder = @"C:\inetpub\FTP\\Estacion4\";
                    break;
                case 5:
                    searchFolder = @"C:\inetpub\FTP\Estacion4_30_p\";
                    break;
                case 6:
                    searchFolder = @"C:\inetpub\FTP\Estacion5\";
                    break;
                case 7:
                     searchFolder = @"C:\inetpub\FTP\Estacion5QR\";
                    break;
                default:
                    break;
            }
            var files = GetFilesOrd(searchFolder);
            string fullpath = searchFolder + files;
            return fullpath;
        }


        private void Busca_ingresa_bd() {
            TraerultimoQR(1);
            salida = Turno_salida();
            if (salida == 1 && GenReporte)
            {
            }
        }

        public string TraerultimoQR(int num)
        {
            Application.DoEvents();
            var filters = new String[] { "csv" };
            lectura = "";
            qr = "";
            estatus = "";
            medicion = 0;
            medicion22 = 0;
            medicion23 = 0;
            lectura22 = "";
            lectura23 = "";
            try
            {
                path_ultimo = Busqueda_de_QR(1); //20180217 OMAR:
                using (StreamReader fielRead = new StreamReader(path_ultimo))
                {
                    String line;
                    while ((line = fielRead.ReadLine()) != null)
                    {
                        string[] datos = line.Split(new char[] { ',' });
                        qr = Limpiarcad(datos[datos.Length - 3]);
                        lectura = Limpiarcad(datos[datos.Length - 2]);
                        estatus = Limpiarcad(datos[datos.Length - 1]);
                    }
                }
                File.Delete(path_ultimo);
                TXTST1QRTR.Text = qr;
                medicion = Convert.ToInt32(lectura);
                //medicion = medicion + 45.7;
                medicion = (medicion * 0.0001) + 45.7;   //20180215 OMAR: PARA CORREGIR FORMATO DEL VALOR MOSTRADO
                lectura = Convert.ToString(medicion);
                txtst1med1.Text = lectura;
                if (estatus.ToUpper() == "OK")
                {
                    IMGST1TR.Image = System.Drawing.Image.FromFile(@"C:\sistema\ok.png");
                }
                else
                {
                    IMGST1TR.Image = System.Drawing.Image.FromFile(@"C:\sistema\ng.png");
                }
                TST1R.Enabled = false;
                TST1R.Enabled = true;
                conexion.Busqueda_de_piezas_est1(qr, lectura, estatus);//20180215 OMAR: PARA ELIMINAR PROCEDIMIENTO DE INGRESO DE PIEZAS
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            try
            {
                path_ultimo = Busqueda_de_QR(2); //20180217 OMAR:
                using (StreamReader fielRead = new StreamReader(path_ultimo))
                {
                    String line;
                    while ((line = fielRead.ReadLine()) != null)
                    {
                        string[] datos = line.Split(new char[] { ',' });
                        qr = Limpiarcad(datos[datos.Length - 5]);
                        lectura = Limpiarcad(datos[datos.Length - 4]);
                        lectura22 = Limpiarcad(datos[datos.Length - 3]);
                        lectura23 = Limpiarcad(datos[datos.Length - 2]);
                        estatus = Limpiarcad(datos[datos.Length - 1]);
                    }
                }
                File.Delete(path_ultimo);
                TXTST2QRTR.Text = qr;
                medicion = Convert.ToDouble(lectura);
                //medicion = medicion + 124.7;
                medicion = (medicion * 0.0001) + 124.7;   //20180215 OMAR: PARA CORREGIR FORMATO DEL VALOR MOSTRADO
                lectura = Convert.ToString(medicion);
                medicion22 = Convert.ToDouble(lectura22);
                //medicion22 = medicion22 + 40.5;
                medicion22 = (medicion22 * 0.0001) + 40.5;  //20180215 OMAR: PARA CORREGIR FORMATO DEL VALOR MOSTRADO
                lectura22 = Convert.ToString(medicion22);
                medicion23 = Convert.ToDouble(lectura23);
                //medicion23 = medicion23 + 40.5;
                medicion23 = (medicion23 * 0.0001) + 40.5;     //20180215 OMAR: PARA CORREGIR FORMATO DEL VALOR MOSTRADO
                lectura23 = Convert.ToString(medicion23);
                txtst2med1.Text = lectura;
                txtst2med2.Text = lectura23;
                txtst2med3.Text = lectura22;
                if (estatus.ToUpper() == "OK")
                {
                    IMGST2TR.Image = System.Drawing.Image.FromFile(@"C:\sistema\ok.png");
                }
                else
                {
                    IMGST2TR.Image = System.Drawing.Image.FromFile(@"C:\sistema\ng.png");
                }
                Tst2r.Enabled = false;
                Tst2r.Enabled = true;
                conexion.Busqueda_de_piezas_est2(qr, lectura, lectura22, lectura23, estatus);//20180215 OMAR: PARA ELIMINAR PROCEDIMIENTO DE INGRESO DE PIEZAS
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            try { 
                path_ultimo = Busqueda_de_QR(3); //20180217 OMAR:
                using (StreamReader fielRead = new StreamReader(path_ultimo))
                {
                    // MessageBox.Show(path_ultimo);
                    String line;
                    while ((line = fielRead.ReadLine()) != null)
                    {
                        string[] datos = line.Split(new char[] { ',' });
                        qr = Limpiarcad(datos[datos.Length - 3]);
                        lectura = Limpiarcad(datos[datos.Length - 2]);
                        estatus = Limpiarcad(datos[datos.Length - 1]);
                    }
                }
                File.Delete(path_ultimo);
                TXTST3QRTR.Text = qr;
                medicion = Convert.ToInt32(lectura);
                //medicion = medicion + 44.5;
                medicion = (medicion * 0.0001) + 44.5;    //20180215 OMAR: PARA CORREGIR FORMATO DEL VALOR MOSTRADO
                lectura = Convert.ToString(medicion);
                txtst3med1.Text = lectura;
                if (estatus.ToUpper() == "OK")
                {
                    IMGST3TR.Image = System.Drawing.Image.FromFile(@"C:\sistema\ok.png");
                }
                else
                {
                    IMGST3TR.Image = System.Drawing.Image.FromFile(@"C:\sistema\ng.png");
                }
                Tst3r.Enabled = false;
                Tst3r.Enabled = true;
                conexion.Busqueda_de_piezas_est3(qr, lectura, estatus);//20180215 OMAR: PARA ELIMINAR PROCEDIMIENTO DE INGRESO DE PIEZAS
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            try { 
                path_ultimo = Busqueda_de_QR(4); //20180217 OMAR:
                using (StreamReader fielRead = new StreamReader(path_ultimo))
                {
                    //MessageBox.Show(path_ultimo);
                    String line;
                    while ((line = fielRead.ReadLine()) != null)
                    {
                        string[] datos = line.Split(new char[] { ',' });
                        qr = Limpiarcad(datos[datos.Length - 3]);
                        lectura = Limpiarcad(datos[datos.Length - 2]);
                        estatus = Limpiarcad(datos[datos.Length - 1]);
                    }
                }
                File.Delete(path_ultimo);
                TXTST4QRTR.Text = qr;
                medicion = Convert.ToInt32(lectura);
                //medicion = medicion + 38.1;
                medicion = (medicion * 0.0001) + 38.1;   //20180215 OMAR: PARA CORREGIR FORMATO DEL VALOR MOSTRADO
                lectura = Convert.ToString(medicion);
                txtst4med1.Text = lectura;
                if (estatus.ToUpper() == "OK")
                {
                    IMGST4TR.Image = System.Drawing.Image.FromFile(@"C:\sistema\ok.png");
                }
                else
                {
                    IMGST4TR.Image = System.Drawing.Image.FromFile(@"C:\sistema\ng.png");
                }
                Tst4r.Enabled = false;
                Tst4r.Enabled = true;
                conexion.Busqueda_de_piezas_est4(qr, lectura, estatus);//20180215 OMAR: PARA ELIMINAR PROCEDIMIENTO DE INGRESO DE PIEZAS
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            try { 
                path_ultimo = Busqueda_de_QR(5); //20180217 OMAR:
                using (StreamReader fielRead = new StreamReader(path_ultimo))
                {
                    // MessageBox.Show(path_ultimo);
                    String line;
                    while ((line = fielRead.ReadLine()) != null)
                    {
                        string[] datos = line.Split(new char[] { ',' });
                        qr = Limpiarcad(datos[datos.Length - 3]);
                        lectura = Limpiarcad(datos[datos.Length - 2]);
                        estatus = Limpiarcad(datos[datos.Length - 1]);
                    }
                }
                File.Delete(path_ultimo);
                medicion = Convert.ToInt32(lectura);
                //medicion = medicion + 44.5;    
                medicion = (medicion * 0.0001) + 44.5;    //20180215 OMAR: PARA CORREGIR FORMATO DEL VALOR MOSTRADO
                lectura = Convert.ToString(medicion);
                conexion.Busqueda_de_piezas_est4_30_p(qr, lectura, estatus);//20180215 OMAR: PARA ELIMINAR PROCEDIMIENTO DE INGRESO DE PIEZAS
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            try { 
                switch (num)
                {
                    case 6:
                         path_ultimo = Busqueda_de_QR(6); //20180217 OMAR:
                         using (StreamReader fielRead = new StreamReader(path_ultimo))
                         {
                             // MessageBox.Show(path_ultimo);
                             String line;
                             while ((line = fielRead.ReadLine()) != null)
                             {
                                 string[] datos = line.Split(new char[] { ',' });
                                 qr = Limpiarcad(datos[datos.Length - 2]);
                                 //lectura = datos[datos.Length - 2];
                                 //estatus = datos[datos.Length - 1];
                             }
                         }
                        //File.Delete(path_ultimo);
                        TextBox16.Text = qr;
                        conexion.Busqueda_de_piezas_est5(qr);//20180215 OMAR: PARA ELIMINAR PROCEDIMIENTO DE INGRESO DE PIEZAS
                        Llenar_pantalla_principal(qr);
                        Terminar_ciclo();
                        break;
                    case 7:
                        //using (StreamWriter fileWrite = new StreamWriter(searchFolder = @"C:\inetpub\Codigostemp\codigosQRtemp7.txt"))
                        {
                            qr = "";
                            path_ultimo = Busqueda_de_QR(7); //20180217 OMAR:
                            using (StreamReader fielRead = new StreamReader(path_ultimo))
                            {
                                String line;
                                while ((line = fielRead.ReadLine()) != null)
                                {
                                    string[] datos = line.Split(new char[] { ',' });
                                    qr = Limpiarcad(datos[datos.Length - 2]);
                                    visual = Limpiarcad(datos[datos.Length - 1]);
                                }
                            }
                        }
                        //File.Delete(path_ultimo);
                        if (visual.ToUpper() == "OK")
                        {
                            pictureBox7.Image = System.Drawing.Image.FromFile(@"C:\sistema\ok.png");
                            Esta5v = 1;
                        }
                        else
                        {
                            pictureBox7.Image = System.Drawing.Image.FromFile(@"C:\sistema\ng.png");
                            pictureBox12.Image = System.Drawing.Image.FromFile(@"C:\sistema\ng1.png");
                            Esta5v = 0;
                        }
                        QR_de_la_busqueda_en_ultima_estacion = qr;
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            GC.Collect();
            return qr;
        }

        public static String GetFilesOrd(String searchFolder)
        {
            try
            {
                DirectoryInfo info = new DirectoryInfo(searchFolder);
                FileInfo[] files = info.GetFiles().OrderBy(p => p.CreationTime).ToArray();
                String ultimo = files[files.Length - 1].ToString();
                return ultimo;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                string ultimo2 = "";
                return ultimo2;
            }
        }

        private void MaterialRaisedButton1_Click(object sender, EventArgs e)
        {
            Salir();
        }

        public void Salir()
        {
           //saliendo = true;
           //this.Close();
        }

        private void Llenar_pantalla_principal(string QR_actual)
        {
            Busca_ingresa_bd();
            this.Refresh();
            if (saliendo == false)//buscar el qr que no este, si no esta. entra directo a poner los datos en la pantalal
            {
                string[] consulta_pantalla_principal = conexion.Busqueda_para_la_pantalla_principal_de_ultima_estacion(QR_actual);
                //textBox16.Text = consulta_pantalla_principal[0];
                TextBox16.Text = QR_actual;
                textBox4.Text = consulta_pantalla_principal[1];
                textBox8.Text = consulta_pantalla_principal[3]; // referente a med22  
                textBox7.Text = consulta_pantalla_principal[5]; // convert to string mosdat[mosdat.length 3]; referente a med21
                textBox6.Text = consulta_pantalla_principal[4]; // convert to string mosdat[mosdat.length 4]; // referente a med23
                                                                // textbox.text convert to string mosdat[mosdat.length 6]; referente a estado2 boton
                textBox11.Text = consulta_pantalla_principal[7]; // convert to string mosdat[mosdat.length 7]; // referente a med3
                                                                 //textbox.text = convert to string mosdat[mosdat.length 8]; // referente a estado3 boton
                textBox10.Text = consulta_pantalla_principal[9]; //convert to string mosdat[mosdat.length 9]; // referente a med4
                                                                 // textbox.text convert to string mosdat[mosdat.length 10]; // referente a estado4 boton
                textBox9.Text = consulta_pantalla_principal[11]; //convert to string mosdat[mosdat.length 11]; // referente a med4_30_piezas
                                                                 //textbox.text convert to string mosdat[mosdat.length 12]; // referente a estado4_30_piezas boton
                                                                 //textbox.text convert to string mosdat[mosdat.length 13]; // referente a gauge boton 
                                                                 //textbox.text convert to string mosdat[mosdat.length 14]; // referente a visual boton 
                                                                 // textbox.text convert to string mosdat[mosdat.length 15]; // referente a estado5
                this.Refresh();
                if (consulta_pantalla_principal[2].ToUpper() == "OK" || consulta_pantalla_principal[2].ToUpper() == "RE") //EST1
                {
                    pictureBox2.Image = System.Drawing.Image.FromFile(@"C:\sistema\ok.png");
                    Esta1 = 1;
                }
                else
                {
                    pictureBox2.Image = System.Drawing.Image.FromFile(@"C:\sistema\ng.png");
                    Esta1 = 0;
                }

                if (consulta_pantalla_principal[6].ToUpper() == "OK"|| consulta_pantalla_principal[6].ToUpper() == "RE")//EST2
                {
                    pictureBox3.Image = System.Drawing.Image.FromFile(@"C:\sistema\ok.png");
                    Esta2 = 1;
                }
                else
                {
                    pictureBox3.Image = System.Drawing.Image.FromFile(@"C:\sistema\ng.png");
                    Esta2 = 0;
                }

                if (consulta_pantalla_principal[8].ToUpper() == "OK"|| consulta_pantalla_principal[8].ToUpper() == "RE")//EST3
                {
                    pictureBox4.Image = System.Drawing.Image.FromFile(@"C:\sistema\ok.png");
                    Esta3 = 1;
                }
                else
                {
                    pictureBox4.Image = System.Drawing.Image.FromFile(@"C:\sistema\ng.png");
                    Esta3 = 0;
                }

                if (consulta_pantalla_principal[10].ToUpper() == "OK")
                {
                    pictureBox5.Image = System.Drawing.Image.FromFile(@"C:\sistema\ok.png");
                    Esta4 = 1;
                }
                else
                {
                    pictureBox5.Image = System.Drawing.Image.FromFile(@"C:\sistema\ng.png");
                    Esta4 = 0;
                }
                pictureBox6.Image = System.Drawing.Image.FromFile(@"C:\sistema\ok.png");
                this.Refresh();
                Busca_ingresa_bd();
                this.Refresh();
                QR_para_comparar = QR_actual;
                Wr("CR");//inicia comunicacion plc
                if (Esta1 == 1 && Esta2 == 1 && Esta3 == 1 && Esta4 == 1)
                {
                   for (int i = counter; i >= 0; i--)
                   {
                       if (i % 2 == 0)
                       {
                           Busca_ingresa_bd();
                       }
                       this.Refresh();
                       txtcountdown.Text = Convert.ToString(i);
                       txtcountdown.Refresh();
                       Thread.Sleep(800);
                   }
                    Wr("WR DM11240.U 111");// avisa al plc que termino el CountDown y que el prejuicio es OK
                }
                else
                {
                    Wr("WR DM11240.U 222");// avisa al plc que termino el CountDown y que el prejuicio es NG
                }
                Console.WriteLine("QR ACTUAL: " + QR_actual);
                Console.WriteLine("empieza el for");Application.DoEvents();
            }
        }

        private void Terminado(object sender, RunWorkerCompletedEventArgs e)
        {
            Console.WriteLine("Termino exitosamente");
        }


        public int Turno_ahora()
        {

            turno = 0;
            DateTime time = DateTime.Now;              // Use current time
            hora_turno = time.Hour;
            int gpo = (((52 * (time.Year - 2018))+ISOnumsemana(time))/4)%3;
            if (hora_turno >= 8 && time.Hour < 21)
            {
                switch (gpo)
                {   
                    case 0:
                        if (time.DayOfWeek <= DayOfWeek.Thursday)
                        {
                            turno = 2;
                        }
                        else
                        {
                            turno = 1;
                        };
                        break;
                    case 1:
                        if (time.DayOfWeek <= DayOfWeek.Thursday)
                        {
                            turno = 1;
                        }
                        else
                        {
                            turno = 3;
                        };
                        break;
                    case 2:
                        if (time.DayOfWeek <= DayOfWeek.Thursday)
                        {
                            turno = 3;
                        }
                        else
                        {
                            turno = 2;
                        };
                        break;
                    default:
                       break;
                }
                //turno = 1;
            }
            else
            {
                switch (gpo)
                {
                    case 0:
                        if (time.DayOfWeek == DayOfWeek.Monday || time.DayOfWeek ==DayOfWeek.Tuesday)
                        {
                            turno = 1;
                        }
                        else
                        {
                            turno = 3;
                        };
                        break;
                    case 1:
                        if (time.DayOfWeek == DayOfWeek.Monday || time.DayOfWeek == DayOfWeek.Tuesday)
                        {
                            turno = 3;
                        }
                        else
                        {
                            turno = 2;
                        };
                        break;
                    case 2:
                        if (time.DayOfWeek == DayOfWeek.Monday || time.DayOfWeek == DayOfWeek.Tuesday)
                        {
                            turno = 2;
                        }
                        else
                        {
                            turno = 1;
                        };
                        break;
                    default:
                        turno = 0;
                        break;
                }
                //turno = 2;
            }
            return turno;
        }

        public static int ISOnumsemana(DateTime time)
        {
            DayOfWeek day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(time);
            if(day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
            {
                time = time.AddDays(3);
            }
            return CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
        }

        public int Turno_salida()
        {
            turno = 0;
            DateTime time = DateTime.Now;              // Use current time
            hora_turno = time.Hour;
            hora_minuto = time.Minute;
            if (hora_turno == 7/* && time.Hour <= 9*/)
            {
                if (hora_minuto >= 45 && hora_minuto <= 52)
                {
                    turno = 1;
                }
                if (hora_minuto >= 53 && hora_minuto <= 59)
                {
                    GenReporte = true;
                };
            }
            if (hora_turno == 20 /* && time.Hour <= 20*/)
            {
                if (hora_minuto >= 45 && hora_minuto <= 52)
                {
                    turno = 1;
                }
                if (hora_minuto >= 53 && hora_minuto <= 59)
                {
                    GenReporte=true;
                }
            }
            return turno;
        } 

 
        private void Principal_FormClosing(object sender, FormClosingEventArgs e)
        {
            SerialPort1.Close();
            if (GenReporte)
            {
                Export_DataTable_PDF.ExportarPDF pdf = new Export_DataTable_PDF.ExportarPDF(Inspector);
                pdf.ShowDialog();
                GenReporte = false;
                conexion.limpiacachesql();

            }
            this.Hide();
            saliendo = true;
            //salir();
        }

        private void Terminar_ciclo()
        {
            Busca_ingresa_bd();
            this.Refresh();
            bandera = 0;
            Console.WriteLine("Empieza busqueda en 7");
            //string estado5;
            while (bandera != 1)
            {
                Wr("CQ");// termina comunicacion con plc por si habia una conexion abierta
                Wr("CR");//inicia comunicacion con plc
                String LECTURARD = Wr("RD DM11260.U").Trim(); //El PLC nos avisa si precionaron el botton OK 111 o el boton ng 222 
                //AutoClosingMessageBox.Show(LECTURARD.ToString()+"*", "NG", 2000);
                if (LECTURARD == "00222")// Se preciono el boton Rojo(NG)
                {
                    pictureBox7.Image = System.Drawing.Image.FromFile(@"C:\sistema\ng.png");
                    pictureBox12.Image = System.Drawing.Image.FromFile(@"C:\sistema\ng1.png");
                    pictureBox7.Refresh();
                    pictureBox12.Refresh();
                    //estado5 = "NG";
                    //wr("WR DM11240.U 222");
                    //AutoClosingMessageBox.Show("Esperando Pieza en el Shut de Rechazo", "NG", 2000);
                    Wr("WR DM11260.U 000");//le indicamos al plc que ya leimos la variable
                }
                if (LECTURARD == "00111")// Se preciono el boton Verde(OK)
                {
                    pictureBox7.Image = System.Drawing.Image.FromFile(@"C:\sistema\ok.png");
                    pictureBox12.Image = System.Drawing.Image.FromFile(@"C:\sistema\okay.png");
                    pictureBox7.Refresh();
                    pictureBox12.Refresh();
                    //estado5 = "OK";
                    //wr("WR DM11240.U 222");
                    //AutoClosingMessageBox.Show("Esperando Pieza en el Shut ", "OK", 2000);
                    Wr("WR DM11260.U 000");//le indicamos al plc que ya leimos la variable
                }
                Application.DoEvents();
                TraerultimoQR(7);
                if (QR_para_comparar == QR_de_la_busqueda_en_ultima_estacion)
                {
                    this.Refresh();
                    bandera = 1;
                    Busca_ingresa_bd();
                }
                if (saliendo)
                {
                    bandera = 1;
                    this.Close();
                }

                salida = Turno_salida();
                if (salida == 1 && GenReporte)
                {
                   Export_DataTable_PDF.ExportarPDF pdf = new Export_DataTable_PDF.ExportarPDF(Inspector);
                   pdf.ShowDialog();
                   MyIni.Write("contador_ok", "0", "Variables");
                   MyIni.Write("contador_ng", "0", "Variables");
                   contador_ng = 0;
                   contador_ok = 0;
                   Txtok.Text = "0";
                   Txtng.Text = "0";
                   Txtok.Refresh();
                   Txtng.Refresh();
                   this.Refresh();
                   GenReporte = false;
                   conexion.limpiacachesql();

                }
            }
             path_ultimo = Busqueda_de_QR(6); //20180217 OMAR:
            //AutoClosingMessageBox.Show("borrando " + path_ultimo.ToString(), "6-7", 2000);
            try
            {
               File.Delete(path_ultimo);
            }
            catch (Exception)
            {
                throw;
            }
            path_ultimo = Busqueda_de_QR(7); //20180217 OMAR:
            //AutoClosingMessageBox.Show("borrando " + path_ultimo.ToString(), "Borrando", 2000);
            try
            {
                File.Delete(path_ultimo);
            }
            catch (Exception)
            {

                throw;
            }
            Console.WriteLine("Termina busqueda");
            consulta_pantalla_principal_del_ultimo_ingreso = conexion.Busqueda_para_la_pantalla_principal_de_ultima_estacion(QR_de_la_busqueda_en_ultima_estacion);
            //File.Delete(@"C:\inetpub\FTP\Estacion5QR\log005.csv");
            //File.Delete(@"C:\inetpub\FTP\Estacion5QR\log051.csv");
            Esta5g = 1;
            if (Esta5g == 1 && Esta5v == 1)
            {
               // esta5 = "ok";
            }
            else
            {
                //esta5 = "ng";
            }
            if (Esta1 == 1 && Esta2 == 1 && Esta3 == 1 && Esta4 == 1 && Esta5g == 1 && Esta5v == 1)
            {
                //contador_ok = contador_ok + 1;
                contador_ok = Convert.ToInt32(MyIni.Read("contador_ok", "Variables")) + 1;
                Txtok.Text = Convert.ToString(contador_ok);
                conexion.Busqueda_de_piezas_est5_final(QR_de_la_busqueda_en_ultima_estacion, "OK", "OK", "OK");
                pictureBox12.Image = System.Drawing.Image.FromFile(@"C:\sistema\okay.png");
                MyIni.Write("contador_ok", Txtok.Text, "Variables");
            }
            else
            {
                //contador_ng = contador_ng + 1;
                contador_ng = Convert.ToInt32(MyIni.Read("contador_ng", "Variables")) + 1;
                MyIni.Write("contador_ng", contador_ng.ToString(), "Variables");
                Txtng.Text = Convert.ToString(contador_ng);
                conexion.Busqueda_de_piezas_est5_final(QR_de_la_busqueda_en_ultima_estacion, "OK", "NG", "NG");
                pictureBox12.Image = System.Drawing.Image.FromFile(@"C:\sistema\ng1.png");
                MyIni.Write("contador_ng", Txtng.Text, "Variables");
            }
            turno_empleado = Turno_ahora();
            if (qr.ToUpper() != "SINCODIGO")
            {
                conexion.Ingresar_piezas_consulta_7(QR_de_la_busqueda_en_ultima_estacion, conexion.Usuario(Turno_ahora()), turno_empleado);
            };
             Busca_ingresa_bd();

            this.Refresh();
            pictureBox6.Image = System.Drawing.Image.FromFile(@"C:\sistema\normal.png");
            pictureBox7.Image = System.Drawing.Image.FromFile(@"C:\sistema\normal.png");
            pictureBox12.Image = System.Drawing.Image.FromFile(@"C:\sistema\normal.png");
            pictureBox2.Image = System.Drawing.Image.FromFile(@"C:\sistema\normal.png");          
            pictureBox3.Image = System.Drawing.Image.FromFile(@"C:\sistema\normal.png");                        
            pictureBox4.Image = System.Drawing.Image.FromFile(@"C:\sistema\normal.png");
            pictureBox5.Image = System.Drawing.Image.FromFile(@"C:\sistema\normal.png");
            txtcountdown.Text = Convert.ToString(counter);
            TextBox16.Text = "";
            textBox4.Text = "";            
            textBox8.Text = "";
            textBox7.Text = "";
            textBox6.Text = "";           
            textBox11.Text = "";            
            textBox10.Text = "";            
            textBox9.Text = "";
            TextBox16.Text = "";
            QR_de_la_busqueda_en_ultima_estacion = "";
            Busca_ingresa_bd();
            this.Refresh();
            GC.Collect();
        }

        public string[] Busqueda_para_la_pantalla_principal(string QR)
        {
        
            if (cn.State.ToString() == "Closed")
            {
                cn.Open();
            }
            string[] resulta = { QR.ToString(), "0", "NG", "0", "0", "0", "NG", "0", "NG", "0", "NG", "0", "NG", "NG", "NG", "NG" };
            if (QR != "SINCODIGO")
            {
                //SqlCommand Comando = new SqlCommand(string.Format("select EST1.QR, EST1.med1, EST1.estado, EST2.med21,EST2.med22, EST2.med23, EST2.estado2,EST3.med3,EST3.estado3,EST4.med4,EST4.estado4, EST4_30_P.med_30_p, EST4_30_P.estado4_30_p, EST5.gauge,EST5.visual,EST5.estado5 from  EST1 LEFT JOIN EST2 ON EST1.QR = EST2.QR2 LEFT JOIN EST3 ON EST1.QR = EST3.QR3 LEFT JOIN EST4 ON EST1.QR = EST4.QR4 LEFT JOIN EST4_30_P ON EST1.QR = EST4_30_P.QR4_30_p LEFT JOIN EST5 ON EST1.QR = EST5.QR5 where EST1.QR = '" + QR + "'"), cn);
               string query = string.Format("SELECT  MAX(CASE WHEN est1.qr IS NULL THEN CASE WHEN est2.qr2 IS NULL THEN CASE WHEN est3.qr3 IS NULL THEN CASE WHEN est4.qr4 IS NULL THEN CASE WHEN est5.qr5 IS NULL  THEN est5.qr5 END ELSE est4.qr4 END ELSE est3.qr3 END ELSE est2.qr2 END ELSE est1.qr END) AS QR, MAX(EST1.med1) AS med1, MAX(EST1.estado) AS estado, MAX(EST2.med21) AS med21, MAX(EST2.med22) AS med22, MAX(EST2.med23) AS med23, MAX(EST2.estado2) AS estado2, MAX(EST3.med3) AS med3, MAX(EST3.estado3) AS estado3, MAX(EST4.med4) AS med4, MAX(EST4.estado4) AS estado4, MAX(EST4_30_P.med_30_p) AS med_30_p,  MAX(EST4_30_P.estado4_30_p) AS estado4_30_p, MAX(EST5.gauge) AS gauge, MAX(EST5.visual) AS visual, MAX(EST5.estado5) AS estado5 FROM (SELECT TOP (1) QR5, gauge, visual, estado5, fecha5 FROM dbo.EST5 AS EST5_1 WHERE (QR5 = '" + QR + "')) AS EST5 FULL OUTER JOIN (SELECT TOP (1) QR, med1, estado, fecha1 FROM dbo.EST1 AS EST1_1 WHERE (QR = '" + QR + "')) AS EST1 ON EST5.QR5 = EST1.QR FULL OUTER JOIN (SELECT TOP (1) QR4_30_p, med_30_p, estado4_30_p, fecha4_30_p FROM dbo.EST4_30_P AS EST4_30_P_1 WHERE (QR4_30_p = '" + QR + "')) AS EST4_30_P ON EST1.QR = EST4_30_P.QR4_30_p FULL OUTER JOIN (SELECT TOP (1) QR4, med4, estado4, fecha4 FROM dbo.EST4 AS EST4_1 WHERE (QR4 = '" + QR + "')) AS EST4 ON EST1.QR = EST4.QR4 FULL OUTER JOIN (SELECT TOP (1) QR3, med3, estado3, fecha3 FROM dbo.EST3 AS EST3_1 WHERE (QR3 = '" + QR + "')) AS EST3 ON EST1.QR = EST3.QR3 FULL OUTER JOIN (SELECT TOP (1) QR2, med21, med22, med23, estado2, fecha2 FROM dbo.EST2 AS EST2_1 WHERE (QR2 = '" + QR + "')) AS EST2 ON EST1.QR = EST2.QR2");
                SqlCommand Comando = new SqlCommand() { };
                Comando.CommandText = query;
                Comando.Connection = cn;
                Comando.CommandTimeout = 1200000;
                Comando.ExecuteNonQuery();
                SqlDataReader reader = Comando.ExecuteReader();
                reader.Read();
                string[] resultado = new string[reader.FieldCount];
                int fieldcount = reader.FieldCount - 1;
                for (int i = 0; i <= fieldcount; i++)
                {
                    resultado[i] = reader[i].ToString();
                }
                reader.Close();
                Comando.Dispose();
                cn.Dispose();
                return resultado;
            }
            else
            {
                return resulta;
            }
        }

        private void MaterialRaisedButton1_MouseClick(object sender, MouseEventArgs e)
        {
            Salir();
        }


        private void TST1R_Tick(object sender, EventArgs e)
        {
            IMGST1TR.Image = System.Drawing.Image.FromFile(@"C:\sistema\normal.png");
            TXTST1QRTR.Text = "";
            txtst1med1.Text = "";
            TST1R.Enabled = false;
        }

        private void Tst2r_Tick(object sender, EventArgs e)
        {
            IMGST2TR.Image = System.Drawing.Image.FromFile(@"C:\sistema\normal.png");
            TXTST2QRTR.Text = "";
            txtst2med1.Text = "";
            txtst2med2.Text = "";
            txtst2med3.Text = "";
            Tst2r.Enabled = false;
        }

        private void Tst3r_Tick(object sender, EventArgs e)
        {
            IMGST3TR.Image = System.Drawing.Image.FromFile(@"C:\sistema\normal.png");
            TXTST3QRTR.Text = "";
            txtst3med1.Text = "";
            Tst3r.Enabled = false;
        }

        private void Tst4r_Tick(object sender, EventArgs e)
        {
            IMGST4TR.Image = System.Drawing.Image.FromFile(@"C:\sistema\normal.png");
            TXTST4QRTR.Text = "";
            txtst4med1.Text = "";
            Tst4r.Enabled = false;
        }

   }//public partial class principal : MaterialSkin.Controls.MaterialForm


}
