﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exedy_Login
{
    public partial class Form1 : MaterialSkin.Controls.MaterialForm
    {
        public Form1()
        {
            InitializeComponent();
            MaterialSkin.MaterialSkinManager skinManager = MaterialSkin.MaterialSkinManager.Instance;
            skinManager.AddFormToManage(this);
            skinManager.Theme = MaterialSkin.MaterialSkinManager.Themes.LIGHT;
            skinManager.ColorScheme = new MaterialSkin.ColorScheme(MaterialSkin.Primary.Blue400, MaterialSkin.Primary.BlueGrey900, MaterialSkin.Primary.BlueGrey500, MaterialSkin.Accent.Green700, MaterialSkin.TextShade.WHITE);
        }

         public static int id;
        
        BD conexion = new BD();
        //principal p = new principal(1);
        DateTime time = DateTime.Now;              // Use current time


        private void Form1_Load(object sender, EventArgs e)
        {
            conexion.Abrir_conexion();
            materialSingleLineTextField1.Focus();
            Principal p = new Principal(2);
            materialSingleLineTextField1.Text = "";
            materialSingleLineTextField2.Text = "";
            this.Hide();  
            p.ShowDialog();
            p.Dispose();
            this.Show();

        }

        public int menu;
        int id_sesion;

        private void MaterialRaisedButton1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                Entrar();
            }
        }


        private void Entrar()
        {
            string user = materialSingleLineTextField1.Text;
            string pass = materialSingleLineTextField2.Text;
            this.id_sesion = conexion.Autentificar_user(user, pass);
            if (conexion.Autentificar_user(user, pass) > 0) 
            {
                id = conexion.Autentificar_user(user, pass);
                Principal p = new Principal(id);
                materialSingleLineTextField1.Text = "";
                materialSingleLineTextField2.Text = "";
                this.Hide();
                p.ShowDialog();
                this.Show();
                //this.Close();
            }
            else
            {
                Exedy_Login.AutoClosingMessageBox.Show("Usuario y contraseña no validos","Acceso denegado",3000);
            }
        }

        private void MaterialRaisedButton1_Click(object sender, EventArgs e)
        {
           Entrar(); 
        }
           
        private void MaterialSingleLineTextField2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                Entrar();
            }
        }
    }
}
