﻿using System;
using System.Windows.Forms;
using System.IO;
using System.Data;
using System.Reflection;
using iTextSharp.text.pdf;
using iTextSharp.text;
using System.Data.SqlClient;

namespace Export_DataTable_PDF
{
    public partial class ExportarPDF : Form
    {
        public ExportarPDF(string user)
        {
            InitializeComponent();
            this.Llenar_usuarios(dataGridView1);
            this.usuario = user;
        }
        DateTime time = DateTime.Now;              // Use current time
        string formato = "MM/dd/yyyy HH:mm:ss";
        public static SqlConnection cn = new SqlConnection("Data Source=192.168.0.100;Initial Catalog=EXEDY;Persist Security Info=True;User ID=admin;Password=megadeth");
        string pathfinal;
        int turno;
        int hora_turno;
        string usuario;
        string turno_dia_noche;
        // Creates or loads an INI file in the same directory as your executable
        // named EXE.ini (where EXE is the name of your executable)
        //var MyIni = new IniFile();
        // Or specify a specific name in the current dir
        //var MyIni = new IniFile("Settings.ini");
        // Or specify a specific name in a specific dir
        Exedy_Login.IniFile MyIni = new Exedy_Login.IniFile(@"C:\sistema\exedi.ini");
        Image ima = Image.GetInstance(@"C:\sistema\exedy.png");
          
        public void Llenar_usuarios(DataGridView dgv)
        {
            SqlDataAdapter da;
            DataTable dt;
            try
            {
                if (cn.State.ToString() == "Closed")
                {
                    cn.Open();
                }
                //dgv.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                dgv.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                if (Turno_ahora() == 1)
                {
                    da = new SqlDataAdapter("select QR, med1, estado,med21, med22, med23, estado2, med3, estado3, med4, estado4,med_30_p,estado4_30_p, gauge, visual,estado5, fecha, Usuario from Consulta7 where fecha between '" + (Convert.ToString(time.Year) + "-" + Convert.ToString(time.Month) + "-" + Convert.ToString(time.Day)) + " 07:45' and '" + (Convert.ToString(time.Year) + "-" + Convert.ToString(time.Month) + "-" + Convert.ToString(time.Day)) + " 20:45' ", cn);
                }
                else
                {
                    //da = new SqlDataAdapter("select QR, med1, estado,med21, med22, med23, estado2, med3, estado3, med4, estado4, gauge, visual, fecha, Usuario from Consulta7 where fecha between '" + (Convert.ToString(time.Year) + "-" + Convert.ToString(time.Month) + "-" + Convert.ToString(time.Day)) + "' and '" + (Convert.ToString(time.Year) + "-" + Convert.ToString(time.Month) + "-" + Convert.ToString(time.Day + 1)) + "' and turno =  '" + turno_ahora() + "'", cn);
                    da = new SqlDataAdapter("select QR, med1, estado,med21, med22, med23, estado2, med3, estado3, med4, estado4,med_30_p,estado4_30_p, gauge, visual,estado5, fecha, Usuario from Consulta7 where fecha between '" + (Convert.ToString(time.Year) + "-" + Convert.ToString(time.Month) + "-" + Convert.ToString(time.Day - 1)) + " 20:45' and '" + (Convert.ToString(time.Year) + "-" + Convert.ToString(time.Month) + "-" + Convert.ToString(time.Day)) + " 07:45' ", cn);
                }
                dt = new DataTable();
                da.Fill(dt);
                dgv.DataSource = dt;
                dgv.AlternatingRowsDefaultCellStyle.BackColor = System.Drawing.Color.LightGray;
                dgv.Columns[0].Width = 430;
                dgv.Columns[0].HeaderText = "QR";
                dgv.Columns[1].HeaderText = "BOSS OD";
                dgv.Columns[1].Width = 300;
                dgv.Columns[2].HeaderText = "JUCIO 1";
                dgv.Columns[2].Width = 250;
                dgv.Columns[3].HeaderText = "PISTON ROOM";
                dgv.Columns[3].Width = 300;
                dgv.Columns[4].HeaderText = "ID TOP BOSS";
                dgv.Columns[4].Width = 300;
                dgv.Columns[5].HeaderText = "ID BOTTOM BOSS";
                dgv.Columns[5].Width = 300;
                dgv.Columns[6].HeaderText = "JUCIO 2";
                dgv.Columns[6].Width = 250;
                dgv.Columns[7].HeaderText = "INNER OD";
                dgv.Columns[7].Width = 300;
                dgv.Columns[8].HeaderText = "JUICIO 3";
                dgv.Columns[8].Width = 250;
                dgv.Columns[9].HeaderText = "BUSH ID";
                dgv.Columns[9].Width = 300;
                dgv.Columns[10].HeaderText = "JUICIO 4";
                dgv.Columns[10].Width = 250;
                dgv.Columns[11].HeaderText = "BUSH ID-50";
                dgv.Columns[11].Width = 300;
                dgv.Columns[12].HeaderText = "JUICIO 4-50";
                dgv.Columns[12].Width = 250;
                dgv.Columns[13].HeaderText = "GAUGE";
                dgv.Columns[13].Width = 250;
                dgv.Columns[14].HeaderText = "VISUAL";
                dgv.Columns[14].Width = 250;
                dgv.Columns[15].HeaderText = "JUICIO 5";
                dgv.Columns[15].Width = 250;
                dgv.Columns[16].HeaderText = "FECHA";
                dgv.Columns[16].Width = 450;
                dgv.Columns[17].HeaderText = "GPO";
                dgv.Columns[17].Width = 200;
            }
            catch (Exception ex)
            {
                Exedy_Login.AutoClosingMessageBox.Show("No se pudo llenar el Datagridview: " + ex.ToString(),"Exportar PDF",2000);
            }
            SqlConnection.ClearAllPools();
        }

        public void Crearpdf(string Usuario_reporte)
        {
            Llenar_usuarios(dataGridView1);
            BackUp();
            try
            {
               //Creating iTextSharp Table from the DataTable data
               float[] columnWidths = { 430, 300, 250, 300, 300, 300, 250, 300, 250, 300, 250, 300, 250, 250, 250, 250, 450, 200 };
               PdfPTable pdfTable = new PdfPTable(columnWidths);
               pdfTable.DefaultCell.Padding = 3;  
               pdfTable.WidthPercentage = 100;    
               pdfTable.HorizontalAlignment = Element.ALIGN_LEFT;
               pdfTable.DefaultCell.BorderWidth = 2;
               dataGridView1.AlternatingRowsDefaultCellStyle.BackColor = System.Drawing.Color.LightGray;
               //Adding Header row
               foreach (DataGridViewColumn column in dataGridView1.Columns)
               {
                   // PdfPCell cell = new PdfPCell(new Phrase(column.HeaderText));
                    PdfPCell cell = new PdfPCell() { };cell.Phrase = new Phrase(column.HeaderText);
                    cell.BackgroundColor = new iTextSharp.text.Color(230, 235, 240);
                   pdfTable.AddCell(cell);
               }
               //Adding DataRow
               foreach (DataGridViewRow row in dataGridView1.Rows)
               {   
                   foreach (DataGridViewCell cell in row.Cells)
                   {
                       PdfPCell columna = new PdfPCell(new Phrase(cell.ToString()));
                       if (row.Index % 2 == 0){ columna.BackgroundColor = new iTextSharp.text.Color(220, 220, 220); } else { columna.BackgroundColor = new iTextSharp.text.Color(255, 255, 255); }
                       //pdfTable.AddCell(cell.Value.ToString());
                       pdfTable.AddCell(columna);
                   }
               }
               string folderPath = pathfinal + @"\";
               if (!Directory.Exists(folderPath))
               {
                  Directory.CreateDirectory(folderPath);
               }
                try
                {
                    using (FileStream stream = new FileStream(folderPath + "Reporte del turno " + Turno_ahora() + ".pdf", FileMode.Create))
                    {
                        Document pdfDoc = new Document(PageSize.LETTER, 5f, 5f, 5f, 5f);
                        PdfWriter.GetInstance(pdfDoc, stream);
                        pdfDoc.AddTitle("Reporte de PDF");
                        pdfDoc.AddCreator("Exedy");
                        pdfDoc.Open();
                        pdfDoc.Add(ima);//agrega imagen
                        pdfDoc.Add(new Paragraph(" "));
                        pdfDoc.Add(new Paragraph("Reporte del día: " + time.ToString(formato)));
                        pdfDoc.Add(new Paragraph(" "));
                        pdfDoc.Add(new Paragraph("Generador por el Grupo: " + Usuario_reporte));
                        pdfDoc.Add(new Paragraph(" "));
                        pdfDoc.Add(new Paragraph(" "));
                        pdfDoc.Add(pdfTable);
                        pdfDoc.Close();
                        stream.Close();
                    }
                    this.Close();
                }
                catch (Exception)
                {
                   this.Close();
                   throw;
                }
            }
            catch (Exception)
            {
                this.Close();
                throw;
            }
        }

        private void BackUp()
        {
            DateTime hoy = DateTime.Now;
            String ruta = @"C:\sistema\reportes\";
            String path = ruta + Convert.ToString(hoy.Year);
            String path_dia;

            if (Directory.Exists(path))
            {
                String path_mes = ruta + Convert.ToString(hoy.Year) + @"\" + Convert.ToString(hoy.Month);
                if (Directory.Exists(path_mes))
                {
                    path_dia = ruta + Convert.ToString(hoy.Year) + @"\" + Convert.ToString(hoy.Month);
                    path_dia = path_dia + @"\" + Convert.ToString(hoy.Day);
                    if (Directory.Exists(path_dia))
                    {
                    }
                    else
                    {
                        DirectoryInfo di = Directory.CreateDirectory(path_dia);
                    }
                }
                else
                {
                    DirectoryInfo di = Directory.CreateDirectory(path_mes);
                    path_dia = ruta + Convert.ToString(hoy.Year) + @"\" + Convert.ToString(hoy.Month);
                    path_dia = @"\" + Convert.ToString(hoy.Day);
                    if (Directory.Exists(path_dia))
                    {
                    }
                    else
                    {
                        DirectoryInfo dai = Directory.CreateDirectory(path_dia);
                    }
                }
            }
            else
            {
                DirectoryInfo di = Directory.CreateDirectory(path);
                String path_mes = ruta + Convert.ToString(hoy.Year) + @"\" + Convert.ToString(hoy.Month);
                if (Directory.Exists(path_mes))
                {
                    path_dia = ruta + Convert.ToString(hoy.Year) + @"\" + Convert.ToString(hoy.Month);
                    path_dia = path_dia + @"\" + Convert.ToString(hoy.Day);
                    if (Directory.Exists(path_dia))
                    {
                    }
                    else
                    {
                        DirectoryInfo diw = Directory.CreateDirectory(path_dia);
                    }
                }
                else
                {
                    DirectoryInfo diw = Directory.CreateDirectory(path_mes);
                    path_dia = ruta + Convert.ToString(hoy.Year) + @"\" + Convert.ToString(hoy.Month);
                    path_dia = path_dia + @"\" + Convert.ToString(hoy.Day);
                    if (Directory.Exists(path_dia))
                    {
                    }
                    else
                    {
                        DirectoryInfo dai = Directory.CreateDirectory(path_dia);
                    }
                }
            }
            pathfinal = path_dia;
        }

        private void BtnExportPdf_Click(object sender, EventArgs e)
        {
            //Creating iTextSharp Table from the DataTable data
            PdfPTable pdfTable = new PdfPTable(dataGridView1.ColumnCount);
            pdfTable.DefaultCell.Padding = 3;
            //pdfTable.WidthPercentage = 100;
            pdfTable.HorizontalAlignment = Element.ALIGN_LEFT;
            pdfTable.DefaultCell.BorderWidth = 1;
            //Adding Header row
            foreach (DataGridViewColumn column in dataGridView1.Columns)
            {
                //PdfPCell cell = new PdfPCell(new Phrase(column.HeaderText));
                PdfPCell cell = new PdfPCell() { };cell.Phrase = new Phrase(column.HeaderText);
                cell.BackgroundColor = new iTextSharp.text.Color(240, 240, 240);
                pdfTable.AddCell(cell);
            }
            //Adding DataRow
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                foreach (DataGridViewCell cell in row.Cells)
                {
                    PdfPCell columna = new PdfPCell(new Phrase(cell.ToString()));
                    if (row.Index % 2 == 0) { columna.BackgroundColor = new iTextSharp.text.Color(120, 120, 120); } else { columna.BackgroundColor = new iTextSharp.text.Color(255, 255, 255); }
                    pdfTable.AddCell(columna);
                    //pdfTable.AddCell(cell.Value.ToString());
                }
            }
            string folderPath = pathfinal + @"\";
            //MessageBox.Show(folderPath);
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            using (FileStream stream = new FileStream(folderPath + "Reporte del turno "+ Turno_ahora() + ".pdf", FileMode.Create))
            {
                Document pdfDoc = new Document(PageSize.LETTER,5f,5f,5f,5f);
                PdfWriter.GetInstance(pdfDoc, stream);
                pdfDoc.AddTitle("Reporte de PDF");
                pdfDoc.AddCreator("Exedy");
                pdfDoc.Open();
                pdfDoc.Add(new Paragraph("Reporte del día: " + time.ToString(formato)));
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(new Paragraph("Generador por el Grupo: " + usuario));
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(new Paragraph(" "));
                pdfDoc.Add(pdfTable);
                pdfDoc.Close();
                stream.Close();
            }
        }

        public int Turno_ahora()
        {
            turno = 0;
            hora_turno = time.Hour;
            if (hora_turno >= 08 && time.Hour <= 21)
            {
                turno = 1;
                turno_dia_noche = "Día";
            }
            else 
            {
                turno = 2;
                turno_dia_noche = "Noche";
            }
            return turno;
        }

        public void Form1_Load(object sender, EventArgs e)
        {
            Llenar_usuarios(dataGridView1);
            BackUp();
            //Creating iTextSharp Table from the DataTable data
            try
            {
                float[] columnWidths = { 430, 300, 250, 300, 300, 300, 250, 300, 250, 300, 250, 300, 250, 250, 250, 250, 450, 200 };
                PdfPTable pdfTable = new PdfPTable(columnWidths);
                pdfTable.DefaultCell.Padding = 3;
                pdfTable.WidthPercentage = 100;
                pdfTable.HorizontalAlignment = Element.ALIGN_CENTER;
                pdfTable.DefaultCell.BorderWidth = 1;
                //Adding Header row
                foreach (DataGridViewColumn column in dataGridView1.Columns)
                {
                    Font f = new Font(2, 6, 0, GrayColor.BLACK);
                    //PdfPCell cell = new PdfPCell(new Phrase(column.HeaderText, f));
                    PdfPCell cell = new PdfPCell() { };
                    cell.Phrase = new Phrase(column.HeaderText, f);
                    //cell.BackgroundColor = GrayColor.GRAY;
                    cell.BackgroundColor = new iTextSharp.text.Color(System.Drawing.Color.RoyalBlue);
                    //cell.BackgroundColor = new iTextSharp.text.Color(240, 240, 240);
                    pdfTable.AddCell(cell);
                }
                //Adding DataRow
                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    foreach (DataGridViewCell cell in row.Cells)
                    {
                        Font f = new Font(2, 6, 0, GrayColor.BLACK);
                        PdfPCell columna = new PdfPCell(new Phrase(cell.Value.ToString(),f));
                        if (cell.RowIndex % 2 != 0) { columna.BackgroundColor = new iTextSharp.text.Color(System.Drawing.Color.LightBlue); } else { columna.BackgroundColor = new iTextSharp.text.Color(System.Drawing.Color.White); };
                        pdfTable.AddCell(columna);
                    }
                }
                string folderPath = pathfinal + @"\";
                if (!Directory.Exists(folderPath))
                {
                    Directory.CreateDirectory(folderPath);
                }
                try
                {
                    Turno_ahora();
                    using (FileStream stream = new FileStream(folderPath + "Reporte del turno " + turno_dia_noche + ".pdf", FileMode.Create))
                    {
                        Document pdfDoc = new Document(PageSize.LETTER);
                        PdfWriter.GetInstance(pdfDoc, stream);
                        pdfDoc.AddTitle("Reporte de PDF");
                        pdfDoc.AddCreator("Exedy");
                        pdfDoc.Open();
                        pdfDoc.Add(ima);  // carga imagen
                        pdfDoc.Add(new Paragraph(" "));
                        pdfDoc.Add(new Paragraph("Reporte del día: " + time.ToString(formato)));
                        //pdfDoc.Add(new Paragraph(" "));
                        pdfDoc.Add(new Paragraph("Generador por el Grupo: " + usuario+ "                                Pzas OK =  " + MyIni.Read("contador_ok", "Variables") + "                 Pzas Ng  = " + MyIni.Read("contador_ng", "Variables")));
                        pdfDoc.Add(new Paragraph(" "));
                        pdfDoc.Add(pdfTable);
                        pdfDoc.Close();
                        stream.Close();
                    }
                    this.Close();
                }
                catch (Exception)
                {
                    Exedy_Login.AutoClosingMessageBox.Show("El documento para generar el reporte esta abierto, por favor cerrarlo","Exportar PDF",3000);
                    this.Close();
                    throw;
                }
            }
            catch (Exception)
            {
                this.Close();
                throw;
            } 
        }
    }
}
