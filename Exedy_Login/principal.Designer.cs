﻿namespace Exedy_Login
{
    partial class Principal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Principal));
            this.SerialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.Txtok = new System.Windows.Forms.TextBox();
            this.materialLabel2 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel3 = new MaterialSkin.Controls.MaterialLabel();
            this.materialRaisedButton1 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtcountdown = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.TXTST1QRTR = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.TXTST2QRTR = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.TXTST3QRTR = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.TXTST4QRTR = new System.Windows.Forms.TextBox();
            this.Txtng = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.TextBox16 = new System.Windows.Forms.TextBox();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.IMGST4TR = new System.Windows.Forms.PictureBox();
            this.IMGST3TR = new System.Windows.Forms.PictureBox();
            this.IMGST2TR = new System.Windows.Forms.PictureBox();
            this.IMGST1TR = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.txtst1med1 = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txtst2med1 = new System.Windows.Forms.TextBox();
            this.txtst2med2 = new System.Windows.Forms.TextBox();
            this.txtst2med3 = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.txtst3med1 = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.txtst4med1 = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.TST1R = new System.Windows.Forms.Timer(this.components);
            this.Tst2r = new System.Windows.Forms.Timer(this.components);
            this.Tst3r = new System.Windows.Forms.Timer(this.components);
            this.Tst4r = new System.Windows.Forms.Timer(this.components);
            this.CmbPuertos = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IMGST4TR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IMGST3TR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IMGST2TR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IMGST1TR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            this.SuspendLayout();
            // 
            // Txtok
            // 
            this.Txtok.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Txtok.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txtok.Location = new System.Drawing.Point(690, 38);
            this.Txtok.Name = "Txtok";
            this.Txtok.ReadOnly = true;
            this.Txtok.Size = new System.Drawing.Size(146, 55);
            this.Txtok.TabIndex = 4;
            this.Txtok.Text = "0";
            this.Txtok.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // materialLabel2
            // 
            this.materialLabel2.AutoSize = true;
            this.materialLabel2.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.materialLabel2.Depth = 0;
            this.materialLabel2.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel2.Location = new System.Drawing.Point(391, 81);
            this.materialLabel2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel2.Name = "materialLabel2";
            this.materialLabel2.Size = new System.Drawing.Size(32, 19);
            this.materialLabel2.TabIndex = 5;
            this.materialLabel2.Text = "OK:";
            // 
            // materialLabel3
            // 
            this.materialLabel3.AutoSize = true;
            this.materialLabel3.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.materialLabel3.Depth = 0;
            this.materialLabel3.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel3.Location = new System.Drawing.Point(608, 81);
            this.materialLabel3.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel3.Name = "materialLabel3";
            this.materialLabel3.Size = new System.Drawing.Size(34, 19);
            this.materialLabel3.TabIndex = 7;
            this.materialLabel3.Text = "NG:";
            // 
            // materialRaisedButton1
            // 
            this.materialRaisedButton1.Depth = 0;
            this.materialRaisedButton1.Location = new System.Drawing.Point(1726, 37);
            this.materialRaisedButton1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialRaisedButton1.Name = "materialRaisedButton1";
            this.materialRaisedButton1.Primary = true;
            this.materialRaisedButton1.Size = new System.Drawing.Size(75, 31);
            this.materialRaisedButton1.TabIndex = 8;
            this.materialRaisedButton1.Text = "Cerrar sesión ";
            this.materialRaisedButton1.UseVisualStyleBackColor = true;
            this.materialRaisedButton1.Click += new System.EventHandler(this.MaterialRaisedButton1_Click);
            this.materialRaisedButton1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MaterialRaisedButton1_MouseClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 22F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label1.Location = new System.Drawing.Point(19, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(140, 36);
            this.label1.TabIndex = 10;
            this.label1.Text = "CÓDIGO:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 22F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label2.Location = new System.Drawing.Point(619, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 36);
            this.label2.TabIndex = 12;
            this.label2.Text = "OK:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label3.Location = new System.Drawing.Point(874, 49);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 33);
            this.label3.TabIndex = 13;
            this.label3.Text = "NG:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label4.Location = new System.Drawing.Point(1148, 40);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 58);
            this.label4.TabIndex = 15;
            this.label4.Text = "COUNT\r\nDOWN\r\n";
            // 
            // txtcountdown
            // 
            this.txtcountdown.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtcountdown.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcountdown.Location = new System.Drawing.Point(1243, 40);
            this.txtcountdown.Multiline = true;
            this.txtcountdown.Name = "txtcountdown";
            this.txtcountdown.ReadOnly = true;
            this.txtcountdown.Size = new System.Drawing.Size(146, 62);
            this.txtcountdown.TabIndex = 14;
            this.txtcountdown.Text = "10";
            this.txtcountdown.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.label5.Location = new System.Drawing.Point(154, 429);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(92, 29);
            this.label5.TabIndex = 22;
            this.label5.Text = "VALOR";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.label6.Location = new System.Drawing.Point(2, 429);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(132, 29);
            this.label6.TabIndex = 21;
            this.label6.Text = "MEDICIÓN";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.label7.Location = new System.Drawing.Point(433, 429);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(92, 29);
            this.label7.TabIndex = 24;
            this.label7.Text = "VALOR";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.label10.Location = new System.Drawing.Point(263, 429);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(132, 29);
            this.label10.TabIndex = 23;
            this.label10.Text = "MEDICIÓN";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.label11.Location = new System.Drawing.Point(691, 429);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(92, 29);
            this.label11.TabIndex = 26;
            this.label11.Text = "VALOR";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.label12.Location = new System.Drawing.Point(535, 429);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(132, 29);
            this.label12.TabIndex = 25;
            this.label12.Text = "MEDICIÓN";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(990, 526);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(0, 29);
            this.label13.TabIndex = 28;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.label14.Location = new System.Drawing.Point(3, 474);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(106, 20);
            this.label14.TabIndex = 27;
            this.label14.Text = "Ø 45.7(±0.05)";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.label15.Location = new System.Drawing.Point(264, 472);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(119, 20);
            this.label15.TabIndex = 29;
            this.label15.Text = "Ø 124.7 (±0.05)";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.label16.Location = new System.Drawing.Point(536, 474);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(142, 20);
            this.label16.TabIndex = 30;
            this.label16.Text = "Ø 44.5(-0.06/-0.12)";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.label18.Location = new System.Drawing.Point(268, 536);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(156, 18);
            this.label18.TabIndex = 32;
            this.label18.Text = "Ø 40.5(+0.034/ -0.005)";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.label19.Location = new System.Drawing.Point(264, 505);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(160, 18);
            this.label19.TabIndex = 33;
            this.label19.Text = "Ø 40.5 (+0.034/ -0.005)";
            // 
            // textBox4
            // 
            this.textBox4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox4.Location = new System.Drawing.Point(150, 474);
            this.textBox4.Multiline = true;
            this.textBox4.Name = "textBox4";
            this.textBox4.ReadOnly = true;
            this.textBox4.Size = new System.Drawing.Size(98, 25);
            this.textBox4.TabIndex = 35;
            this.textBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBox6
            // 
            this.textBox6.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox6.Location = new System.Drawing.Point(423, 536);
            this.textBox6.Multiline = true;
            this.textBox6.Name = "textBox6";
            this.textBox6.ReadOnly = true;
            this.textBox6.Size = new System.Drawing.Size(98, 25);
            this.textBox6.TabIndex = 36;
            this.textBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBox7
            // 
            this.textBox7.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox7.Location = new System.Drawing.Point(423, 505);
            this.textBox7.Multiline = true;
            this.textBox7.Name = "textBox7";
            this.textBox7.ReadOnly = true;
            this.textBox7.Size = new System.Drawing.Size(98, 25);
            this.textBox7.TabIndex = 37;
            this.textBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBox8
            // 
            this.textBox8.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox8.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textBox8.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox8.Location = new System.Drawing.Point(423, 472);
            this.textBox8.Name = "textBox8";
            this.textBox8.ReadOnly = true;
            this.textBox8.Size = new System.Drawing.Size(98, 25);
            this.textBox8.TabIndex = 38;
            this.textBox8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBox9
            // 
            this.textBox9.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox9.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox9.Location = new System.Drawing.Point(946, 558);
            this.textBox9.Multiline = true;
            this.textBox9.Name = "textBox9";
            this.textBox9.ReadOnly = true;
            this.textBox9.Size = new System.Drawing.Size(98, 25);
            this.textBox9.TabIndex = 39;
            this.textBox9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBox10
            // 
            this.textBox10.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox10.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox10.Location = new System.Drawing.Point(946, 497);
            this.textBox10.Multiline = true;
            this.textBox10.Name = "textBox10";
            this.textBox10.ReadOnly = true;
            this.textBox10.Size = new System.Drawing.Size(98, 25);
            this.textBox10.TabIndex = 40;
            this.textBox10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBox11
            // 
            this.textBox11.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox11.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox11.Location = new System.Drawing.Point(685, 474);
            this.textBox11.Multiline = true;
            this.textBox11.Name = "textBox11";
            this.textBox11.ReadOnly = true;
            this.textBox11.Size = new System.Drawing.Size(98, 25);
            this.textBox11.TabIndex = 41;
            this.textBox11.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 22F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label21.Location = new System.Drawing.Point(196, 854);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(132, 36);
            this.label21.TabIndex = 43;
            this.label21.Text = "CÓDIGO";
            // 
            // TXTST1QRTR
            // 
            this.TXTST1QRTR.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TXTST1QRTR.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXTST1QRTR.Location = new System.Drawing.Point(135, 893);
            this.TXTST1QRTR.Multiline = true;
            this.TXTST1QRTR.Name = "TXTST1QRTR";
            this.TXTST1QRTR.ReadOnly = true;
            this.TXTST1QRTR.Size = new System.Drawing.Size(248, 36);
            this.TXTST1QRTR.TabIndex = 42;
            this.TXTST1QRTR.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 22F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label22.Location = new System.Drawing.Point(627, 854);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(132, 36);
            this.label22.TabIndex = 45;
            this.label22.Text = "CÓDIGO";
            // 
            // TXTST2QRTR
            // 
            this.TXTST2QRTR.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TXTST2QRTR.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXTST2QRTR.Location = new System.Drawing.Point(562, 893);
            this.TXTST2QRTR.Multiline = true;
            this.TXTST2QRTR.Name = "TXTST2QRTR";
            this.TXTST2QRTR.ReadOnly = true;
            this.TXTST2QRTR.Size = new System.Drawing.Size(243, 36);
            this.TXTST2QRTR.TabIndex = 44;
            this.TXTST2QRTR.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 22F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label23.Location = new System.Drawing.Point(1045, 854);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(132, 36);
            this.label23.TabIndex = 47;
            this.label23.Text = "CÓDIGO";
            // 
            // TXTST3QRTR
            // 
            this.TXTST3QRTR.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TXTST3QRTR.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXTST3QRTR.Location = new System.Drawing.Point(980, 893);
            this.TXTST3QRTR.Multiline = true;
            this.TXTST3QRTR.Name = "TXTST3QRTR";
            this.TXTST3QRTR.ReadOnly = true;
            this.TXTST3QRTR.Size = new System.Drawing.Size(246, 36);
            this.TXTST3QRTR.TabIndex = 46;
            this.TXTST3QRTR.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 22F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label24.Location = new System.Drawing.Point(1512, 854);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(132, 36);
            this.label24.TabIndex = 49;
            this.label24.Text = "CÓDIGO";
            // 
            // TXTST4QRTR
            // 
            this.TXTST4QRTR.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TXTST4QRTR.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TXTST4QRTR.Location = new System.Drawing.Point(1447, 893);
            this.TXTST4QRTR.Multiline = true;
            this.TXTST4QRTR.Name = "TXTST4QRTR";
            this.TXTST4QRTR.ReadOnly = true;
            this.TXTST4QRTR.Size = new System.Drawing.Size(251, 36);
            this.TXTST4QRTR.TabIndex = 48;
            this.TXTST4QRTR.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Txtng
            // 
            this.Txtng.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Txtng.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txtng.Location = new System.Drawing.Point(946, 40);
            this.Txtng.Multiline = true;
            this.Txtng.Name = "Txtng";
            this.Txtng.ReadOnly = true;
            this.Txtng.Size = new System.Drawing.Size(146, 62);
            this.Txtng.TabIndex = 6;
            this.Txtng.Text = "0";
            this.Txtng.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.AutoSize = true;
            this.button1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(0, 1041);
            this.button1.Margin = new System.Windows.Forms.Padding(0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(10313, 3179);
            this.button1.TabIndex = 51;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = false;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.label17.Location = new System.Drawing.Point(796, 474);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(148, 18);
            this.label17.TabIndex = 31;
            this.label17.Text = "Ø 38.1(+0.07/+0.045)";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.label8.Location = new System.Drawing.Point(797, 429);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(132, 29);
            this.label8.TabIndex = 19;
            this.label8.Text = "MEDICIÓN";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.label20.Location = new System.Drawing.Point(800, 536);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(120, 18);
            this.label20.TabIndex = 34;
            this.label20.Text = "Ø 44.5(+0/- 0.15)";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.label9.Location = new System.Drawing.Point(956, 429);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(92, 29);
            this.label9.TabIndex = 20;
            this.label9.Text = "VALOR";
            // 
            // TextBox16
            // 
            this.TextBox16.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TextBox16.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBox16.Location = new System.Drawing.Point(165, 36);
            this.TextBox16.Multiline = true;
            this.TextBox16.Name = "TextBox16";
            this.TextBox16.ReadOnly = true;
            this.TextBox16.Size = new System.Drawing.Size(395, 64);
            this.TextBox16.TabIndex = 64;
            this.TextBox16.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // pictureBox12
            // 
            this.pictureBox12.Image = global::Exedy_Login.Properties.Resources.normal;
            this.pictureBox12.Location = new System.Drawing.Point(1632, 236);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(248, 238);
            this.pictureBox12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox12.TabIndex = 65;
            this.pictureBox12.TabStop = false;
            // 
            // IMGST4TR
            // 
            this.IMGST4TR.Image = ((System.Drawing.Image)(resources.GetObject("IMGST4TR.Image")));
            this.IMGST4TR.Location = new System.Drawing.Point(1512, 722);
            this.IMGST4TR.Name = "IMGST4TR";
            this.IMGST4TR.Size = new System.Drawing.Size(132, 119);
            this.IMGST4TR.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.IMGST4TR.TabIndex = 63;
            this.IMGST4TR.TabStop = false;
            // 
            // IMGST3TR
            // 
            this.IMGST3TR.Image = ((System.Drawing.Image)(resources.GetObject("IMGST3TR.Image")));
            this.IMGST3TR.Location = new System.Drawing.Point(1045, 722);
            this.IMGST3TR.Name = "IMGST3TR";
            this.IMGST3TR.Size = new System.Drawing.Size(132, 119);
            this.IMGST3TR.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.IMGST3TR.TabIndex = 62;
            this.IMGST3TR.TabStop = false;
            // 
            // IMGST2TR
            // 
            this.IMGST2TR.Image = ((System.Drawing.Image)(resources.GetObject("IMGST2TR.Image")));
            this.IMGST2TR.Location = new System.Drawing.Point(633, 722);
            this.IMGST2TR.Name = "IMGST2TR";
            this.IMGST2TR.Size = new System.Drawing.Size(132, 119);
            this.IMGST2TR.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.IMGST2TR.TabIndex = 61;
            this.IMGST2TR.TabStop = false;
            // 
            // IMGST1TR
            // 
            this.IMGST1TR.Image = global::Exedy_Login.Properties.Resources.normal;
            this.IMGST1TR.Location = new System.Drawing.Point(196, 722);
            this.IMGST1TR.Name = "IMGST1TR";
            this.IMGST1TR.Size = new System.Drawing.Size(132, 119);
            this.IMGST1TR.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.IMGST1TR.TabIndex = 60;
            this.IMGST1TR.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox7.Image")));
            this.pictureBox7.Location = new System.Drawing.Point(1322, 185);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(248, 238);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox7.TabIndex = 59;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
            this.pictureBox6.Location = new System.Drawing.Point(1056, 185);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(248, 238);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox6.TabIndex = 58;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(796, 185);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(248, 238);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox5.TabIndex = 57;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(264, 185);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(257, 238);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox3.TabIndex = 56;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(0, 185);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(248, 238);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox2.TabIndex = 55;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(535, 185);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(248, 238);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox4.TabIndex = 54;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(0, 24);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1919, 1017);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox13
            // 
            this.pictureBox13.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox13.Image")));
            this.pictureBox13.Location = new System.Drawing.Point(87, 722);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(312, 238);
            this.pictureBox13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox13.TabIndex = 66;
            this.pictureBox13.TabStop = false;
            // 
            // pictureBox14
            // 
            this.pictureBox14.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox14.Image")));
            this.pictureBox14.Location = new System.Drawing.Point(535, 722);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(312, 238);
            this.pictureBox14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox14.TabIndex = 67;
            this.pictureBox14.TabStop = false;
            // 
            // pictureBox15
            // 
            this.pictureBox15.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox15.Image")));
            this.pictureBox15.Location = new System.Drawing.Point(961, 722);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(312, 238);
            this.pictureBox15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox15.TabIndex = 68;
            this.pictureBox15.TabStop = false;
            // 
            // pictureBox16
            // 
            this.pictureBox16.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox16.Image")));
            this.pictureBox16.Location = new System.Drawing.Point(1406, 722);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(312, 238);
            this.pictureBox16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox16.TabIndex = 69;
            this.pictureBox16.TabStop = false;
            // 
            // txtst1med1
            // 
            this.txtst1med1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtst1med1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtst1med1.Location = new System.Drawing.Point(350, 944);
            this.txtst1med1.Multiline = true;
            this.txtst1med1.Name = "txtst1med1";
            this.txtst1med1.ReadOnly = true;
            this.txtst1med1.Size = new System.Drawing.Size(98, 25);
            this.txtst1med1.TabIndex = 71;
            this.txtst1med1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.label25.Location = new System.Drawing.Point(3, 949);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(191, 20);
            this.label25.TabIndex = 70;
            this.label25.Text = "BOSS OD  Ø 45.7(±0.05) ";
            // 
            // txtst2med1
            // 
            this.txtst2med1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtst2med1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtst2med1.Location = new System.Drawing.Point(796, 945);
            this.txtst2med1.Multiline = true;
            this.txtst2med1.Name = "txtst2med1";
            this.txtst2med1.ReadOnly = true;
            this.txtst2med1.Size = new System.Drawing.Size(98, 25);
            this.txtst2med1.TabIndex = 77;
            this.txtst2med1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtst2med2
            // 
            this.txtst2med2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtst2med2.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtst2med2.Location = new System.Drawing.Point(796, 978);
            this.txtst2med2.Multiline = true;
            this.txtst2med2.Name = "txtst2med2";
            this.txtst2med2.ReadOnly = true;
            this.txtst2med2.Size = new System.Drawing.Size(98, 25);
            this.txtst2med2.TabIndex = 76;
            this.txtst2med2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtst2med3
            // 
            this.txtst2med3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtst2med3.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtst2med3.Location = new System.Drawing.Point(796, 1008);
            this.txtst2med3.Multiline = true;
            this.txtst2med3.Name = "txtst2med3";
            this.txtst2med3.ReadOnly = true;
            this.txtst2med3.Size = new System.Drawing.Size(98, 25);
            this.txtst2med3.TabIndex = 75;
            this.txtst2med3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.label26.Location = new System.Drawing.Point(478, 982);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(277, 20);
            this.label26.TabIndex = 74;
            this.label26.Text = "ID TOP BOSS Ø 40.5 (+0.034/ -0.005)";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.label27.Location = new System.Drawing.Point(482, 1013);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(312, 20);
            this.label27.TabIndex = 73;
            this.label27.Text = "ID BOTTOM BOSS Ø 40.5 (+0.034/ -0.005)";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.label28.Location = new System.Drawing.Point(478, 949);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(234, 20);
            this.label28.TabIndex = 72;
            this.label28.Text = "PISTON ROOM Ø 124.7 (±0.05)";
            // 
            // txtst3med1
            // 
            this.txtst3med1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtst3med1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtst3med1.Location = new System.Drawing.Point(1226, 948);
            this.txtst3med1.Multiline = true;
            this.txtst3med1.Name = "txtst3med1";
            this.txtst3med1.ReadOnly = true;
            this.txtst3med1.Size = new System.Drawing.Size(98, 25);
            this.txtst3med1.TabIndex = 79;
            this.txtst3med1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.label29.Location = new System.Drawing.Point(924, 953);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(224, 20);
            this.label29.TabIndex = 78;
            this.label29.Text = "INNER OD Ø 44.5(-0.06/-0.12)";
            // 
            // txtst4med1
            // 
            this.txtst4med1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtst4med1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtst4med1.Location = new System.Drawing.Point(1691, 949);
            this.txtst4med1.Multiline = true;
            this.txtst4med1.Name = "txtst4med1";
            this.txtst4med1.ReadOnly = true;
            this.txtst4med1.Size = new System.Drawing.Size(98, 25);
            this.txtst4med1.TabIndex = 81;
            this.txtst4med1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.label30.Location = new System.Drawing.Point(1341, 953);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(230, 20);
            this.label30.TabIndex = 80;
            this.label30.Text = "BUSH ID Ø 38.1(+0.07/+0.045)";
            // 
            // TST1R
            // 
            this.TST1R.Interval = 10000;
            this.TST1R.Tick += new System.EventHandler(this.TST1R_Tick);
            // 
            // Tst2r
            // 
            this.Tst2r.Interval = 10000;
            this.Tst2r.Tick += new System.EventHandler(this.Tst2r_Tick);
            // 
            // Tst3r
            // 
            this.Tst3r.Interval = 10000;
            this.Tst3r.Tick += new System.EventHandler(this.Tst3r_Tick);
            // 
            // Tst4r
            // 
            this.Tst4r.Interval = 10000;
            this.Tst4r.Tick += new System.EventHandler(this.Tst4r_Tick);
            // 
            // CmbPuertos
            // 
            this.CmbPuertos.FormattingEnabled = true;
            this.CmbPuertos.Location = new System.Drawing.Point(575, 24);
            this.CmbPuertos.Name = "CmbPuertos";
            this.CmbPuertos.Size = new System.Drawing.Size(121, 21);
            this.CmbPuertos.TabIndex = 82;
            this.CmbPuertos.TabStop = false;
            this.CmbPuertos.Visible = false;
            // 
            // Principal
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(1386, 788);
            this.Controls.Add(this.CmbPuertos);
            this.Controls.Add(this.txtst4med1);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.txtst3med1);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.txtst2med1);
            this.Controls.Add(this.txtst2med2);
            this.Controls.Add(this.txtst2med3);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.txtst1med1);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.TXTST4QRTR);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.TXTST3QRTR);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.TXTST2QRTR);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.TXTST1QRTR);
            this.Controls.Add(this.IMGST4TR);
            this.Controls.Add(this.pictureBox16);
            this.Controls.Add(this.IMGST3TR);
            this.Controls.Add(this.pictureBox15);
            this.Controls.Add(this.IMGST2TR);
            this.Controls.Add(this.pictureBox14);
            this.Controls.Add(this.IMGST1TR);
            this.Controls.Add(this.pictureBox13);
            this.Controls.Add(this.pictureBox12);
            this.Controls.Add(this.TextBox16);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox11);
            this.Controls.Add(this.textBox10);
            this.Controls.Add(this.textBox9);
            this.Controls.Add(this.textBox8);
            this.Controls.Add(this.textBox7);
            this.Controls.Add(this.textBox6);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtcountdown);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.materialRaisedButton1);
            this.Controls.Add(this.materialLabel3);
            this.Controls.Add(this.Txtng);
            this.Controls.Add(this.materialLabel2);
            this.Controls.Add(this.Txtok);
            this.Controls.Add(this.pictureBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(1364, 0);
            this.Name = "Principal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Principal_FormClosing);
            this.Load += new System.EventHandler(this.Principal_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IMGST4TR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IMGST3TR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IMGST2TR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IMGST1TR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        public System.Windows.Forms.TextBox Txtok;
        private MaterialSkin.Controls.MaterialLabel materialLabel2;
        private MaterialSkin.Controls.MaterialLabel materialLabel3;
        private MaterialSkin.Controls.MaterialRaisedButton materialRaisedButton1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtcountdown;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox TXTST1QRTR;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox TXTST2QRTR;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox TXTST3QRTR;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox TXTST4QRTR;
        public System.Windows.Forms.TextBox Txtng;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox IMGST2TR;
        private System.Windows.Forms.PictureBox IMGST3TR;
        private System.Windows.Forms.PictureBox IMGST4TR;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.PictureBox IMGST1TR;
        private System.Windows.Forms.TextBox TextBox16;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.PictureBox pictureBox16;
        private System.Windows.Forms.TextBox txtst1med1;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtst2med1;
        private System.Windows.Forms.TextBox txtst2med2;
        private System.Windows.Forms.TextBox txtst2med3;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox txtst3med1;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox txtst4med1;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Timer TST1R;
        private System.Windows.Forms.Timer Tst2r;
        private System.Windows.Forms.Timer Tst3r;
        private System.Windows.Forms.Timer Tst4r;
        private System.Windows.Forms.ComboBox CmbPuertos;
        private System.IO.Ports.SerialPort SerialPort1;
        //private System.Windows.Forms.TextBox txtTx;

    }
}