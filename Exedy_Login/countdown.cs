﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exedy_Login// SOLO FUE DE PRUEBA NO SE USA
{
    public partial class countdown : MaterialSkin.Controls.MaterialForm
    {
        public countdown()
        {
            InitializeComponent();
            MaterialSkin.MaterialSkinManager skinManager = MaterialSkin.MaterialSkinManager.Instance;
            skinManager.AddFormToManage(this);
            skinManager.Theme = MaterialSkin.MaterialSkinManager.Themes.LIGHT;
            skinManager.ColorScheme = new MaterialSkin.ColorScheme(MaterialSkin.Primary.Blue400, MaterialSkin.Primary.BlueGrey900, MaterialSkin.Primary.BlueGrey500, MaterialSkin.Accent.Green700, MaterialSkin.TextShade.WHITE);
        }


        private int counter = 10;
        private void countdown_Load(object sender, EventArgs e)
        {
            countdown_form();
        }


        public void countdown_form()
        {
            counter = 10;
            timer1 = new System.Windows.Forms.Timer();
            timer1.Tick += new EventHandler(timer1_Tick);
            timer1.Interval = 1000; // 1 second
            timer1.Start();
            label1.Text = counter.ToString();
            //Console.WriteLine(textBox5.Text);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            counter--;
            if (counter == 0)
            {
                timer1.Stop();
                //SendKeys.SendWait("{ENTER}");
                //terminar_ciclo();
                this.Close();
            }
            label1.Text = counter.ToString();
            //Console.WriteLine(textBox5.Text);
        }
    }
}
