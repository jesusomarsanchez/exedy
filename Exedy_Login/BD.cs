﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exedy_Login
{
    class BD
    {
        public static SqlConnection cn = new SqlConnection("Data Source=192.168.0.100;Initial Catalog=EXEDY;Persist Security Info=True;User ID=admin;Password=megadeth");
        DateTime time = DateTime.Now;              // Use current time
        string formato = "MM/dd/yyyy HH:mm:ss.sss";
        DataGridView dataGridView1 = new DataGridView();
        public void Abrir_conexion()
        {
            try
            {
                if (cn.State.ToString() == "Closed")
                {
                 cn.Open();
                }
            }
            catch (Exception)
            {
                Exedy_Login.AutoClosingMessageBox.Show("No se encuentra la conexion con la BD, por favor notifique a un administrador","",3000);
                throw;
            }
        }

        public int Ingresar_piezas_consulta_7(string QR, string inspector, int turno)
        {
            try
            {
                if (cn.State.ToString() == "Closed")
                {
                    cn.Open();
                }
                var Comandob = new SqlCommand() { };
                Comandob.CommandText = string.Format("select * from  Consulta7 where QR = '" + QR + "'");
                Comandob.Connection = cn;
                Comandob.CommandTimeout = 120000;
                Comandob.ExecuteNonQuery();
                string query="";
                DateTime time = DateTime.Now;
                SqlDataReader reader = Comandob.ExecuteReader();
                if (reader.Read())
                {
                   reader.Close();
                   query = "Update Consulta7 SET qr=c7.qr, med1 = c7.med1,estado = c7.estado, med21=c7.med21,med22=c7.med22, med23=c7.med23, estado2=c7.estado2, med3=c7.med3, estado3=c7.estado3, med4=c7.med4, estado4=c7.estado4,med_30_p=c7.med_30_p, estado4_30_p=c7.estado4_30_p, gauge= c7.gauge, visual=c7.visual, estado5=c7.estado5, Usuario= c7.usuario, turno=c7.turno, fecha=c7.fecha from (SELECT '"+QR+"' as qr, EST1.med1, EST1.estado, EST2.med21, EST2.med22, EST2.med23, EST2.estado2,EST3.med3 , EST3.estado3, EST4.med4 , EST4.estado4, EST4_30_P.med_30_p, EST4_30_P.estado4_30_p ,EST5.gauge, EST5.visual, EST5.estado5, '" + inspector + "' as Usuario, " + turno + " as turno, '" + time.ToString(formato) + "' as fecha  FROM (SELECT TOP (1) * FROM  EST5 WHERE(QR5 = '" + QR + "')) AS EST5 FULL OUTER JOIN(SELECT TOP (1) * FROM EST1 WHERE (QR = '" + QR + "')) AS EST1 ON EST5.QR5 = EST1.QR FULL OUTER JOIN(SELECT TOP (1) * FROM EST4_30_P WHERE (QR4_30_p = '" + QR + "')) AS EST4_30_P ON EST1.QR = EST4_30_P.QR4_30_p FULL OUTER JOIN (SELECT TOP (1) * FROM EST4 WHERE (QR4 = '" + QR + "')) AS EST4 ON EST1.QR = EST4.QR4 FULL OUTER JOIN (SELECT TOP (1) * FROM EST3 WHERE (QR3 = '" + QR + "')) AS EST3 ON EST1.QR = EST3.QR3 FULL OUTER JOIN (SELECT TOP (1) * FROM EST2 WHERE (QR2 = '" + QR + "')) AS EST2 ON EST1.QR = EST2.QR2) c7 where consulta7.qr = '"+QR+"'";
                   var Comando = new SqlCommand() { };
                   Comando.CommandText = query;
                   Comando.Connection = cn;
                   Comando.CommandTimeout = 1200000;
                   Comando.ExecuteNonQuery();
                   Comando.Connection.Close();
                   Comando.Dispose();
                   return 0;
                }
                else
                {
                   reader.Close();
                   query = "INSERT INTO Consulta7 SELECT  MAX(CASE WHEN est1.qr IS NULL THEN CASE WHEN est2.qr2 IS NULL THEN CASE WHEN est3.qr3 IS NULL THEN CASE WHEN est4.qr4 IS NULL THEN CASE WHEN est5.qr5 IS NULL  THEN est5.qr5 END ELSE est4.qr4 END ELSE est3.qr3 END ELSE est2.qr2 END ELSE est1.qr END) AS QR, MAX(EST1.med1) AS med1, MAX(EST1.estado) AS estado, MAX(EST2.med21) AS med21, MAX(EST2.med22) AS med22, MAX(EST2.med23) AS med23, MAX(EST2.estado2) AS estado2, MAX(EST3.med3) AS med3, MAX(EST3.estado3) AS estado3, MAX(EST4.med4) AS med4, MAX(EST4.estado4) AS estado4, MAX(EST4_30_P.med_30_p) AS med_30_p,  MAX(EST4_30_P.estado4_30_p) AS estado4_30_p, MAX(EST5.gauge) AS gauge, MAX(EST5.visual) AS visual, MAX(EST5.estado5) AS estado5,'" + time.ToString(formato) + "'as fecha, '" + inspector + "'as usuario, '" + turno + "'as turno FROM (SELECT TOP (1) QR5, gauge, visual, estado5, fecha5 FROM dbo.EST5 AS EST5_1 WHERE (QR5 = '" + QR + "')) AS EST5 FULL OUTER JOIN (SELECT TOP (1) QR, med1, estado, fecha1 FROM dbo.EST1 AS EST1_1 WHERE (QR = '" + QR + "')) AS EST1 ON EST5.QR5 = EST1.QR FULL OUTER JOIN (SELECT TOP (1) QR4_30_p, med_30_p, estado4_30_p, fecha4_30_p FROM dbo.EST4_30_P AS EST4_30_P_1 WHERE (QR4_30_p = '" + QR + "')) AS EST4_30_P ON EST1.QR = EST4_30_P.QR4_30_p FULL OUTER JOIN (SELECT TOP (1) QR4, med4, estado4, fecha4 FROM dbo.EST4 AS EST4_1 WHERE (QR4 = '" + QR + "')) AS EST4 ON EST1.QR = EST4.QR4 FULL OUTER JOIN (SELECT TOP (1) QR3, med3, estado3, fecha3 FROM dbo.EST3 AS EST3_1 WHERE (QR3 = '" + QR + "')) AS EST3 ON EST1.QR = EST3.QR3 FULL OUTER JOIN (SELECT TOP (1) QR2, med21, med22, med23, estado2, fecha2 FROM dbo.EST2 AS EST2_1 WHERE (QR2 = '" + QR + "')) AS EST2 ON EST1.QR = EST2.QR2";
                   SqlCommand Comando = new SqlCommand() { };
                   Comando.CommandText = query;
                   Comando.Connection = cn;
                   Comando.CommandTimeout = 1200000;
                   Comando.ExecuteNonQuery();
                   Comando.Connection.Close();
                   Comando.Dispose();
                   return 0;
                }
            }
            catch (Exception ex)
            {
                AutoClosingMessageBox.Show("Error en insert" + ex.ToString(),"Insert Consulta 7",2000);
                throw;
            }
        }

        public Int32 limpiacachesql()
        {
            try
            {
                if (cn.State.ToString() == "Closed")
                {
                    cn.Open();
                }
                SqlCommand Comando = new SqlCommand(string.Format("BEGIN TRY EXEC sys.sp_configure N'show advanced options', N'1'  RECONFIGURE WITH OVERRIDE; EXEC sys.sp_configure N'max server memory (MB)', N'100' RECONFIGURE WITH OVERRIDE  EXEC sys.sp_configure N'show advanced options', N'0'  RECONFIGURE WITH OVERRIDE WAITFOR DELAY '00:01:50' END TRY BEGIN CATCH END CATCH BEGIN TRY EXEC sys.sp_configure N'show advanced options', N'1'  RECONFIGURE WITH OVERRIDE EXEC sys.sp_configure N'max server memory (MB)', N'2000' RECONFIGURE WITH OVERRIDE  EXEC sys.sp_configure N'show advanced options', N'0'  RECONFIGURE WITH OVERRIDE END TRY BEGIN CATCH END CATCH"), cn);
                SqlDataReader reader = Comando.ExecuteReader();
                reader.Read();
                reader.Close();
                Comando.Dispose();
            }
            catch (Exception)
            {
                return 1;
                throw;
            }
            try
            {
                SqlCommand Comando = new SqlCommand(string.Format("    DBCC FREESYSTEMCACHE('All') WITH NO_INFOMSGS    DBCC FREESESSIONCACHE WITH NO_INFOMSGS    DBCC FREEPROCCACHE WITH NO_INFOMSGS    DECLARE @intDBID INTEGER SET @intDBID = (SELECT dbid FROM master.dbo.sysdatabases WHERE name = 'Exedy')    DBCC FLUSHPROCINDB(@intDBID) WITH NO_INFOMSGS   CHECKPOINT   DBCC DROPCLEANBUFFERS WITH NO_INFOMSGS"), cn);
                SqlDataReader reader = Comando.ExecuteReader();
                reader.Read();
                reader.Close();
                Comando.Dispose();

            }
            catch(Exception)
            {
                return 2;
                throw;
            }
            return 0;
        }//public Int32 limpiacachesql()

        public string[] Busqueda_para_la_pantalla_principal(string QR)
        {
            string[] resulta = { QR.ToString(), "0", "NG", "0", "0", "0", "NG", "0", "NG", "0", "NG", "0", "NG", "NG", "NG", "NG" };
            try
            {
                if (cn.State.ToString() == "Closed")
                {
                    cn.Open();
                }
                //SqlCommand Comando = new SqlCommand(string.Format("select EST1.QR, EST1.med1, EST1.estado, EST2.med21,EST2.med22, EST2.med23, EST2.estado2,EST3.med3,EST3.estado3,EST4.med4,EST4.estado4, EST4_30_P.med_30_p, EST4_30_P.estado4_30_p, EST5.gauge,EST5.visual,EST5.estado5 from  EST1 LEFT JOIN EST2 ON EST1.QR = EST2.QR2 LEFT JOIN EST3 ON EST1.QR = EST3.QR3 LEFT JOIN EST4 ON EST1.QR = EST4.QR4 LEFT JOIN EST4_30_P ON EST1.QR = EST4_30_P.QR4_30_p LEFT JOIN EST5 ON EST1.QR = EST5.QR5 where EST1.QR = '" + QR + "'"), cn);
                SqlCommand Comando = new SqlCommand(string.Format("SELECT  MAX(CASE WHEN est1.qr IS NULL THEN CASE WHEN est2.qr2 IS NULL THEN CASE WHEN est3.qr3 IS NULL THEN CASE WHEN est4.qr4 IS NULL THEN CASE WHEN est5.qr5 IS NULL  THEN est5.qr5 END ELSE est4.qr4 END ELSE est3.qr3 END ELSE est2.qr2 END ELSE est1.qr END) AS QR, MAX(EST1.med1) AS med1, MAX(EST1.estado) AS estado, MAX(EST2.med21) AS med21, MAX(EST2.med22) AS med22, MAX(EST2.med23) AS med23, MAX(EST2.estado2) AS estado2, MAX(EST3.med3) AS med3, MAX(EST3.estado3) AS estado3, MAX(EST4.med4) AS med4, MAX(EST4.estado4) AS estado4, MAX(EST4_30_P.med_30_p) AS med_30_p,  MAX(EST4_30_P.estado4_30_p) AS estado4_30_p, MAX(EST5.gauge) AS gauge, MAX(EST5.visual) AS visual, MAX(EST5.estado5) AS estado5 FROM (SELECT TOP (1) QR5, gauge, visual, estado5, fecha5 FROM dbo.EST5 AS EST5_1 WHERE (QR5 = '" + QR + "')) AS EST5 FULL OUTER JOIN (SELECT TOP (1) QR, med1, estado, fecha1 FROM dbo.EST1 AS EST1_1 WHERE (QR = '" + QR + "')) AS EST1 ON EST5.QR5 = EST1.QR FULL OUTER JOIN (SELECT TOP (1) QR4_30_p, med_30_p, estado4_30_p, fecha4_30_p FROM dbo.EST4_30_P AS EST4_30_P_1 WHERE (QR4_30_p = '" + QR + "')) AS EST4_30_P ON EST1.QR = EST4_30_P.QR4_30_p FULL OUTER JOIN (SELECT TOP (1) QR4, med4, estado4, fecha4 FROM dbo.EST4 AS EST4_1 WHERE (QR4 = '" + QR + "')) AS EST4 ON EST1.QR = EST4.QR4 FULL OUTER JOIN (SELECT TOP (1) QR3, med3, estado3, fecha3 FROM dbo.EST3 AS EST3_1 WHERE (QR3 = '" + QR + "')) AS EST3 ON EST1.QR = EST3.QR3 FULL OUTER JOIN (SELECT TOP (1) QR2, med21, med22, med23, estado2, fecha2 FROM dbo.EST2 AS EST2_1 WHERE (QR2 = '" + QR + "')) AS EST2 ON EST1.QR = EST2.QR2"), cn);
                SqlDataReader reader = Comando.ExecuteReader();
                reader.Read();
                string[] resul = new string[reader.FieldCount];
                int fieldcount = reader.FieldCount - 1;
                for (int i = 0; i <= fieldcount; i++)
                {
                    resul[i] = reader[i].ToString();
                }
                reader.Close();
                Comando.Dispose();
                return resul;
            }
            catch (Exception)
            {
                return resulta;
                throw;
            }
        }//public string[] busqueda_para_la_pantalla_principal(string QR)

        public string[] Busqueda_para_la_pantalla_principal_de_ultima_estacion(string QR)
        {
            if (cn.State.ToString() == "Closed")
            {
                cn.Open();
            }
            string[] resulta= { QR.ToString(), "0", "NG", "0", "0", "0", "NG", "0", "NG", "0", "NG", "0", "NG", "NG", "NG", "NG" } ;
            try
            {
                if (QR != "SINCODIGO")
                {
                    //SqlCommand Comando = new SqlCommand(string.Format("select EST1.QR, EST1.med1, EST1.estado, EST2.med21,EST2.med22, EST2.med23, EST2.estado2,EST3.med3,EST3.estado3,EST4.med4,EST4.estado4, EST4_30_P.med_30_p, EST4_30_P.estado4_30_p, EST5.gauge,EST5.visual,EST5.estado5 from  EST1 LEFT JOIN EST2 ON EST1.QR = EST2.QR2 LEFT JOIN EST3 ON EST1.QR = EST3.QR3 LEFT JOIN EST4 ON EST1.QR = EST4.QR4 LEFT JOIN EST4_30_P ON EST1.QR = EST4_30_P.QR4_30_p LEFT JOIN EST5 ON EST1.QR = EST5.QR5 where EST1.QR = '" + QR + "'"), cn);
                    SqlCommand Comando = new SqlCommand() { };
                    Comando.CommandText = string.Format("SELECT  MAX(CASE WHEN est1.qr IS NULL THEN CASE WHEN est2.qr2 IS NULL THEN CASE WHEN est3.qr3 IS NULL THEN CASE WHEN est4.qr4 IS NULL THEN CASE WHEN est5.qr5 IS NULL  THEN est5.qr5 END ELSE est4.qr4 END ELSE est3.qr3 END ELSE est2.qr2 END ELSE est1.qr END) AS QR, MAX(EST1.med1) AS med1, MAX(EST1.estado) AS estado, MAX(EST2.med21) AS med21, MAX(EST2.med22) AS med22, MAX(EST2.med23) AS med23, MAX(EST2.estado2) AS estado2, MAX(EST3.med3) AS med3, MAX(EST3.estado3) AS estado3, MAX(EST4.med4) AS med4, MAX(EST4.estado4) AS estado4, MAX(EST4_30_P.med_30_p) AS med_30_p,  MAX(EST4_30_P.estado4_30_p) AS estado4_30_p, MAX(EST5.gauge) AS gauge, MAX(EST5.visual) AS visual, MAX(EST5.estado5) AS estado5 FROM (SELECT TOP (1) QR5, gauge, visual, estado5, fecha5 FROM dbo.EST5 AS EST5_1 WHERE (QR5 = '" + QR + "')) AS EST5 FULL OUTER JOIN (SELECT TOP (1) QR, med1, estado, fecha1 FROM dbo.EST1 AS EST1_1 WHERE (QR = '" + QR + "')) AS EST1 ON EST5.QR5 = EST1.QR FULL OUTER JOIN (SELECT TOP (1) QR4_30_p, med_30_p, estado4_30_p, fecha4_30_p FROM dbo.EST4_30_P AS EST4_30_P_1 WHERE (QR4_30_p = '" + QR + "')) AS EST4_30_P ON EST1.QR = EST4_30_P.QR4_30_p FULL OUTER JOIN (SELECT TOP (1) QR4, med4, estado4, fecha4 FROM dbo.EST4 AS EST4_1 WHERE (QR4 = '" + QR + "')) AS EST4 ON EST1.QR = EST4.QR4 FULL OUTER JOIN (SELECT TOP (1) QR3, med3, estado3, fecha3 FROM dbo.EST3 AS EST3_1 WHERE (QR3 = '" + QR + "')) AS EST3 ON EST1.QR = EST3.QR3 FULL OUTER JOIN (SELECT TOP (1) QR2, med21, med22, med23, estado2, fecha2 FROM dbo.EST2 AS EST2_1 WHERE (QR2 = '" + QR + "')) AS EST2 ON EST1.QR = EST2.QR2");
                    Comando.Connection = cn;
                    Comando.CommandTimeout = 120000;
                    Comando.ExecuteNonQuery();
                    SqlDataReader reader = Comando.ExecuteReader();
                    if (reader.Read())
                    {
                        string[] resultado = new string[reader.FieldCount];
                        int fieldcount = reader.FieldCount - 1;
                        for (int i = 0; i <= fieldcount; i++)
                        {
                            resultado[i] = reader[i].ToString();
                        }
                        reader.Close();
                        return resultado;
                    }
                    else
                    {
                        reader.Close();
                        return resulta;
                    }
                }
                else { return resulta; }
            }
            catch (Exception)
            {
                return resulta;
                throw;
            }
        }

        public string Busqueda_de_piezas_est1(string QR, string med1, string estado)
        {
            string correcto = "";
            try
            {
                if (cn.State.ToString() == "Closed")
                {
                    cn.Open();
                }
                string query = "SELECT fecha1 from est1 where QR = '" + QR + "'";
                using (SqlCommand cmd = new SqlCommand(query, cn))
                {
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        cmd.CommandTimeout = 9999999;
                        DateTime fech = Convert.ToDateTime(reader[0].ToString());
                        reader.Close();
                        //busqueda_de_piezas_est1_fecha1(QR, med1, estado, fech);
                        Update_piezas_est1_fecha1(QR, med1, estado);
                        correcto = "ng";
                    }
                    else
                    {
                        reader.Close();
                        Ingresar_piezas_est1(QR, med1, estado);
                        correcto = "ok";
                    }
                    cmd.Dispose();
                }//using (SqlCommand cmd = new SqlCommand(query, cn))
                SqlConnection.ClearAllPools();
                return correcto;
            }
            catch (Exception)
            {
                return correcto;
                throw;
            }
        }

        public string Update_piezas_est1_fecha1(string QR, string med1, string estado)
        {
            string correcto = "";
            try
            {
                if (cn.State.ToString() == "Closed")
                {
                    cn.Open();
                }
                DateTime time = DateTime.Now;              // Use current time
                string query = "update est1 set med1 = '" + med1 + "', estado = '" + estado + "', fecha1 = '" + time.ToString(formato) + "' where QR = '" + QR + "'";
                using (SqlCommand cmd = new SqlCommand(query, cn))
                {
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        cmd.CommandTimeout = 9999999;
                        reader.Close();
                        correcto = "ng";
                    }
                    else
                    {
                        reader.Close();
                        correcto = "ok";
                    }
                    cmd.Dispose();
                }
                SqlConnection.ClearAllPools();
                return correcto;
            }
            catch (Exception)
            {
                return correcto;
                throw;
            }
        }

        public int Ingresar_piezas_est1(string QR, string med1, string estado)
        {
            try
            {
                if (cn.State.ToString() == "Closed")
                {
                    cn.Open();
                }
                DateTime time = DateTime.Now;              // Use current time
                SqlCommand Comando = new SqlCommand() { };
                Comando.CommandText = string.Format("INSERT INTO est1(QR, med1, estado, fecha1) values  ('{0}', '{1}', '{2}', '{3}')", QR, med1, estado, time.ToString(formato));
                Comando.Connection = cn;
                Comando.CommandTimeout = 1200000;
                Comando.ExecuteNonQuery();
                Comando.Connection.Close();
                Comando.Dispose();
                return 0;
            }
            catch (Exception)
            {
               return 0;
               throw;
            }
        }//public int ingresar_piezas_est1(string QR, string med1, string estado)

        public string Busqueda_de_piezas_est2(string QR2, string med21, string med22, string med23, string estado2)
        {
            string correcto = "";
            try
            {
                if (cn.State.ToString() == "Closed")
                {
                    cn.Open();
                }
                string query = "SELECT fecha2 from est2 where QR2 = '" + QR2 + "'";
                using (SqlCommand cmd = new SqlCommand(query, cn))
                {
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        reader.Close();
                        //busqueda_de_piezas_est2_fecha2(QR2, med21, med22, med23, estado2);
                        Update_piezas_est2_fecha2(QR2, med21, med22, med23, estado2);
                        cmd.CommandTimeout = 9999999;
                        correcto = "ng";
                        //reader.Close();omar 20180227
                    }
                    else
                    {
                        reader.Close();
                        Ingresar_piezas_est2(QR2, med21, med22, med23, estado2);
                        correcto = "ok";
                    }
                    cmd.Dispose();
                }
                SqlConnection.ClearAllPools();
                return correcto;
            }
            catch (Exception)
            {
                return correcto;
                throw;
            }
        }

        public string Update_piezas_est2_fecha2(string QR2, string med21, string med22, string med23, string estado2)
        {
            string correcto = "";
            try
            {
                if (cn.State.ToString() == "Closed")
                {
                    cn.Open();
                }
                DateTime time = DateTime.Now;              // Use current time
                string query = "update est2 set med21 = '" + med21 + "', med22 = '" + med22 + "', med23 = '" + med23 + "', estado2 = '" + estado2 + "', fecha2 = '" + time.ToString(formato) + "' where QR2 = '" + QR2 + "'";
                using (SqlCommand cmd = new SqlCommand(query, cn))
                {
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        cmd.CommandTimeout = 9999999;
                        reader.Close();
                        correcto = "ng";
                        //reader.Close();  omar 20180227
                    }
                    else
                    {
                        reader.Close();
                        correcto = "ok";
                    }
                    cmd.Dispose();
                }
                correcto = "ok";
                SqlConnection.ClearAllPools();
                return correcto;
            }
            catch (Exception)
            {
                return correcto;
                throw;
            }
        }//public string update_piezas_est2_fecha2(string QR2, string med21, string med22, string med23, string estado2)

        public int Ingresar_piezas_est2(string QR2, string med21, string med22, string med23, string estado2)
        {
            try
            {
                if (cn.State.ToString() == "Closed")
                {
                   cn.Open();
                }
                DateTime time = DateTime.Now;              // Use current time
                string query = string.Format("INSERT INTO EST2(QR2 ,med21 ,med22 ,med23, estado2 ,fecha2) values  ('{0}', '{1}', '{2}', '{3}','{4}','{5}')", QR2, med21, med22, med23, estado2, time.ToString(formato));
                SqlCommand Comando = new SqlCommand() { };
                Comando.CommandText = query;
                Comando.Connection = cn;
                Comando.CommandTimeout = 1200000;
                Comando.ExecuteNonQuery();
                Comando.Connection.Close();
                Comando.Dispose();
                return 0;
            }
            catch (Exception)
            {
                return 0;
                throw;
            }
        }//public int ingresar_piezas_est2(string QR2, string med21, string med22, string med23, string estado2)

        public string Busqueda_de_piezas_est3(string QR3, string med3, string estado3)
        {
            string correcto = "";
            try
            {
                if (cn.State.ToString() == "Closed")
                {
                    cn.Open();
                }
                string query = "SELECT fecha3 from est3 where QR3 = '" + QR3 + "'";
                using (SqlCommand cmd = new SqlCommand(query, cn))
                {
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        cmd.CommandTimeout = 9999999;
                        DateTime fech = Convert.ToDateTime(reader[0].ToString());
                        //MessageBox.Show(Convert.ToString(fech));
                        reader.Close();
                        //busqueda_de_piezas_est3_fecha3(QR3, med3, estado3, fech);
                        Update_piezas_est3_fecha3(QR3, med3, estado3);
                        correcto = "ng";
                    }
                    else
                    {
                        reader.Close();
                        Ingresar_piezas_est3(QR3, med3, estado3);
                        correcto = "ok";
                    }
                    cmd.Dispose();
                }
                //cn.Dispose();
                SqlConnection.ClearAllPools();
                return correcto;
            }
            catch (Exception)
            {
                return correcto;
                throw;
            }
        }

        public string Update_piezas_est3_fecha3(string QR3, string med3, string estado3)
        {
            string correcto = "";
            try
            {
                if (cn.State.ToString() == "Closed")
                {
                    cn.Open();
                }
                DateTime time = DateTime.Now;              // Use current time
                string query = "update est3 set med3 = '" + med3 + "', estado3 = '" + estado3 + "', fecha3 = '" + time.ToString(formato) + "' where QR3 = '" + QR3 + "'";
                using (SqlCommand cmd = new SqlCommand(query, cn))
                {
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        cmd.CommandTimeout = 9999999;
                        reader.Close();
                        correcto = "ng";
                    }
                    else
                    {
                        reader.Close();
                        correcto = "ok";
                    }
                    cmd.Dispose();
                }
                SqlConnection.ClearAllPools();
                return correcto;
            }
            catch (Exception)
            {
                return correcto;
                throw;
            }
        }//public string update_piezas_est3_fecha3(string QR3, string med3, string estado3)

        public int Ingresar_piezas_est3(string QR3, string med3, string estado3)
        {
            try
            {
                if (cn.State.ToString() == "Closed")
                {
                    cn.Open();
                }
                DateTime time = DateTime.Now;              // Use current time
                string query = string.Format("INSERT INTO est3(QR3, med3, estado3, fecha3) values  ('{0}', '{1}', '{2}', '{3}')", QR3, med3, estado3, time.ToString(formato)); 
                SqlCommand Comando = new SqlCommand() { };
                Comando.CommandText = query;
                Comando.Connection = cn;
                Comando.CommandTimeout = 1200000;
                Comando.ExecuteNonQuery();
                Comando.Connection.Close();
                Comando.Dispose();
                return 0;
            }
            catch (Exception)
            {
                return 0;
                throw;
            }
        }//public int ingresar_piezas_est3(string QR3, string med3, string estado3)

        public string Busqueda_de_piezas_est4(string QR4, string med4, string estado4)
        {
            string correcto = "";
            try
            {
                if (cn.State.ToString() == "Closed")
                {
                    cn.Open();
                }
                string query = "SELECT fecha4 from est4 where QR4 = '" + QR4 + "'";
                using (SqlCommand cmd4 = new SqlCommand(query, cn))
                {
                    SqlDataReader reader4 = cmd4.ExecuteReader();
                    if (reader4.Read())
                    {
                        cmd4.CommandTimeout = 9999999;
                        DateTime fech = Convert.ToDateTime(reader4[0].ToString());
                        reader4.Close();
                        Update_piezas_est4_fecha4(QR4, med4, estado4);
                        correcto = "ng";
                    }
                    else
                    {
                        reader4.Close();
                        Ingresar_piezas_est4(QR4, med4, estado4);
                        correcto = "ok";
                    }
                    cmd4.Dispose();
                }
                SqlConnection.ClearAllPools();
                return correcto;
            }
            catch (Exception)
            {
                return correcto;
                throw;
            }
        }//public string busqueda_de_piezas_est4(string QR4, string med4, string estado4)

        public string Update_piezas_est4_fecha4(string QR4, string med4, string estado4)
        {
            string correcto = "";
            try
            {
                if (cn.State.ToString() == "Closed")
                {
                    cn.Open();
                }
                DateTime time = DateTime.Now;              // Use current time
                string query = "update est4 set med4 = '" + med4 + "', estado4 = '" + estado4 + "', fecha4 = '" + time.ToString(formato) + "' where QR4 = '" + QR4 + "'";
                using (SqlCommand cmd = new SqlCommand(query, cn))
                {
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {

                        cmd.CommandTimeout = 9999999;
                        reader.Close();
                        correcto = "ng";
                    }
                    else
                    {
                        reader.Close();
                        correcto = "ok";
                    }
                    cmd.Dispose();
                }
                SqlConnection.ClearAllPools();
                return correcto;
            }
            catch (Exception)
            {
                return correcto;
                throw;
            }
        }//public string update_piezas_est4_fecha4(string QR4, string med4, string estado4)

        public int Ingresar_piezas_est4(string QR4, string med4, string estado4)
        {
            try
            {
                if (cn.State.ToString() == "Closed")
                {
                    cn.Open();
                }
                DateTime time = DateTime.Now;              // Use current time
                string query = string.Format("INSERT INTO EST4(QR4,med4,estado4,fecha4) values  ('{0}', '{1}', '{2}', '{3}')", QR4, med4, estado4, time.ToString(formato));
                SqlCommand Comando = new SqlCommand() { };
                Comando.CommandText = query;
                Comando.Connection = cn;
                Comando.CommandTimeout = 1200000;
                Comando.ExecuteNonQuery();
                Comando.Connection.Close();
                Comando.Dispose();
                //cn.Dispose();
                return 0;
            }
            catch (Exception)
            {
                return 0;
                throw;
            }
        }//public int ingresar_piezas_est4(string QR4, string med4, string estado4)

        public string Busqueda_de_piezas_est4_30_p(string QR4_30_p, string med_30_p, string estado4_30_p)
        {
            string correcto = "";
            try
            {
                if (cn.State.ToString() == "Closed")
                {
                    cn.Open();
                }
                string query = "SELECT fecha4_30_p from est4_30_p where QR4_30_p = '" + QR4_30_p + "'";
                using (SqlCommand cmd = new SqlCommand(query, cn))
                {
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        cmd.CommandTimeout = 9999999;
                        DateTime fech = Convert.ToDateTime(reader[0].ToString());
                        reader.Close();
                        Update_piezas_est4_30_p__fecha4_30_p(QR4_30_p, med_30_p, estado4_30_p);
                        correcto = "ng";
                    }
                    else
                    {
                        reader.Close();
                        Ingresar_piezas_est4_30_p(QR4_30_p, med_30_p, estado4_30_p);
                        correcto = "ok";
                    }
                    cmd.Dispose();
                }
                SqlConnection.ClearAllPools();
                return correcto;
            }
            catch (Exception)
            {
                return correcto;
                throw;
            }
        }//public string busqueda_de_piezas_est4_30_p(string QR4_30_p, string med_30_p, string estado4_30_p)

        public string Update_piezas_est4_30_p__fecha4_30_p(string QR4_30_p, string med_30_p, string estado4_30_p)
        {
            string correcto = "";
            try
            {
                if (cn.State.ToString() == "Closed")
                {
                    cn.Open();
                }
                DateTime time = DateTime.Now;              // Use current time
                string query = "update est4_30_p set med_30_p = '" + med_30_p + "', estado4_30_p = '" + estado4_30_p + "', fecha4_30_p = '" + time.ToString(formato) + "' where QR4_30_p = '" + QR4_30_p + "'";
                using (SqlCommand cmd = new SqlCommand(query, cn))
                {
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        reader.Close();
                        cmd.CommandTimeout = 9999999;
                        correcto = "ng";
                    }
                    else
                    {
                        reader.Close();
                        correcto = "ok";
                    }
                    cmd.Dispose();
                }
                SqlConnection.ClearAllPools();
                return correcto;
            }
            catch (Exception)
            {
                return correcto;
                throw;
            }
        }//public string update_piezas_est4_30_p__fecha4_30_p(string QR4_30_p, string med_30_p, string estado4_30_p)

        public int Ingresar_piezas_est4_30_p(string QR4_30_p, string med_30_p, string estado4_30_p)
        {
            try
            {
                if (cn.State.ToString() == "Closed")
                {
                    cn.Open();
                }
                DateTime time = DateTime.Now;              // Use current time
                string query = string.Format("INSERT INTO est4_30_p(QR4_30_p, med_30_p, estado4_30_p, fecha4_30_p) values  ('{0}', '{1}', '{2}', '{3}')", QR4_30_p, med_30_p, estado4_30_p, time.ToString(formato));
                SqlCommand Comando = new SqlCommand() { };
                Comando.CommandText = query;
                Comando.Connection = cn;
                Comando.CommandTimeout = 1200000;
                Comando.ExecuteNonQuery();
                Comando.Connection.Close();
                Comando.Dispose();
                return 0;
            }
            catch (Exception)
            {
                return 0;
                throw;
            }
        }//public int ingresar_piezas_est4_30_p(string QR4_30_p, string med_30_p, string estado4_30_p)

        public string Busqueda_de_piezas_est5(string QR5)
        {
            try
            {
                string correcto = "";
                if (cn.State.ToString() == "Closed")
                {
                    cn.Open();
                }
                string query = "SELECT fecha5 from est5 where QR5 = '" + QR5 + "'";
                using (SqlCommand cmd2 = new SqlCommand(query, cn))
                {
                    SqlDataReader reader2 = cmd2.ExecuteReader();
                    if (reader2.Read())
                    {
                        cmd2.CommandTimeout = 9999999;
                        reader2.Close();
                        correcto = "ng";
                    }
                    else
                    {
                        reader2.Close();
                        Ingresar_piezas_est5(QR5);
                        correcto = "ok";
                    }
                    cmd2.Dispose();
                }
                SqlConnection.ClearAllPools();
                return correcto;
            }
            catch (Exception ex)
            {
                AutoClosingMessageBox.Show(ex.ToString(),"busqueda_de_piezas_est5", 2000);
                throw;
            }
        }//public string busqueda_de_piezas_est5(string QR5)

        public int Ingresar_piezas_est5(string QR5)
        {
            try
            {
                if (cn.State.ToString() == "Closed")
                {
                    cn.Open();
                }
                DateTime time = DateTime.Now;              // Use current time
                string query = string.Format("INSERT INTO est5(QR5, gauge, visual, estado5, fecha5) values  ('{0}', '{1}', '{2}', '{3}','{4}' )", QR5, "OK", null, null, time.ToString(formato));
                SqlCommand Comando = new SqlCommand() { };
                Comando.CommandText = query;
                Comando.Connection = cn;
                Comando.CommandTimeout = 1200000;
                Comando.ExecuteNonQuery();
                Comando.Connection.Close();
                Comando.Dispose();
                return 0;
            }
            catch (Exception)
            {
                return 0;
                throw;
            }
        }// public int ingresar_piezas_est5(string QR5)

        public string Busqueda_de_piezas_est5_final(string QR5, string gauge, string visual, string estado5)
        {
            string correcto = "";
            try
            {
                if (cn.State.ToString() == "Closed")
                {
                    cn.Open();
                }
                string query = "SELECT fecha5 from est5 where QR5 = '" + QR5 + "'";
                using (SqlCommand cmd3 = new SqlCommand(query, cn))
                {
                    SqlDataReader reader3 = cmd3.ExecuteReader();
                    if (reader3.Read())
                    {
                        cmd3.CommandTimeout = 9999999;
                        reader3.Close();
                        Update_piezas_est5_final(QR5, gauge, visual, estado5);
                        correcto = "ng";
                    }
                    else
                    {
                        reader3.Close();
                        correcto = "ok";
                    }
                    cmd3.Dispose();
                }
                SqlConnection.ClearAllPools();
                return correcto;
            }
            catch (Exception)
            {
                return correcto;
                throw;
            }
        }//public string busqueda_de_piezas_est5_final(string QR5, string gauge, string visual, string estado5)

        public string Update_piezas_est5_final(string QR5, string gauge, string visual, string estado5)
        {
            string correcto = "";
            try
            {
                if (cn.State.ToString() == "Closed")
                {
                    cn.Open();
                }
                DateTime time = DateTime.Now;              // Use current time
                string query = "update est5 set gauge = '" + gauge + "', visual = '" + visual + "', estado5 = '" + estado5 + "', fecha5 = '" + time.ToString(formato) + "' where QR5 = '" + QR5 + "'";
                using (SqlCommand cmd = new SqlCommand(query, cn))
                {
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        cmd.CommandTimeout = 9999999;
                        reader.Close();
                        correcto = "ng";
                    }
                    else
                    {
                        reader.Close();
                        correcto = "ok";
                    }
                    cmd.Dispose();
                }
                SqlConnection.ClearAllPools();
                return correcto;
            }
            catch (Exception)
            {
                return correcto;
                throw;
            }
        }//public string update_piezas_est5_final(string QR5, string gauge, string visual, string estado5)

        public int Autentificar(String pUsuarios, String pContraseña)
        {
            if (cn.State.ToString() == "Closed")
            {
                cn.Open();
            }
            int resultado = 0;
            string query = string.Format("Select Niv_Sec From USUARIOS Where Id_Honda = '{0}' and Contraseña = '{1}' ", pUsuarios, pContraseña);
            SqlCommand Comando = new SqlCommand() { };
            Comando.CommandText = query;
            Comando.Connection = cn;
            Comando.CommandTimeout = 1200000;
            SqlDataReader reader = Comando.ExecuteReader();
            if (reader.Read())
            {
                string Sec = reader[0].ToString();
                int Niv = Convert.ToInt32(Sec);
                if (Niv == 1)
                {
                    resultado = 1;
                }
                else
                {
                    resultado = 2;
                }
            }
            else
            {
                resultado=0;
            }
            Comando.Dispose();
            reader.Close();
            return resultado;
        }//public int Autentificar(String pUsuarios, String pContraseña)

        public int Autentificar_user(String pUsuarios, String pContraseña)
        {
            int resultado = 1;
            try
            {
                if (cn.State.ToString() == "Closed")
                {
                    cn.Open();
                }
                SqlCommand comando2 = new SqlCommand(string.Format(
                    "Select idusuario From USUARIOS Where idexedy = '{0}' and password = '{1}' ", pUsuarios, pContraseña), cn);
                SqlDataReader reader4 = comando2.ExecuteReader();
                if (reader4.Read())
                {
                    comando2.CommandTimeout = 1200000;
                    string Sec = reader4[0].ToString();
                    int Niv = Convert.ToInt32(Sec);
                    resultado = Niv;
                }
                else
                {
                    resultado = 0;
                }
                comando2.Dispose();
                reader4.Close();
                return resultado;
            }
            catch (Exception)
            {
                return resultado;
                throw;
            }
        }//public int Autentificar_user(String pUsuarios, String pContraseña)

        public string Usuario(int usuario)
        {
            string empleado="";
            if (cn.State.ToString() == "Closed")
            {
                cn.Open();
            }
            try
            {
                //string query = "select Nombre + ' ' +  segundo_nombre + ' ' + aparterno + ' ' + amaterno + ' '  as Usuario From USUARIOS where idusuario = '" + usuario + "'";
                string query = "select Nombre + ' ' +  segundo_nombre + ' ' + aparterno + ' ' + amaterno + ' '  as Usuario From USUARIOS where idexedy = '" + usuario + "'";
                using (SqlCommand cmd = new SqlCommand(query, cn))
                {
                    SqlDataReader reader = cmd.ExecuteReader();
                    reader.Read();
                    empleado = reader[0].ToString();
                    cmd.CommandTimeout = 9999999;
                    reader.Close();
                    cmd.Dispose();
                }
                SqlConnection.ClearAllPools();
                return empleado;
            }
            catch (System.Exception ex)
            {
                AutoClosingMessageBox.Show(ex.Message," Usuario",2000);
                SqlConnection.ClearAllPools();
                empleado = "No existe";
                return empleado;
            }

        }

        public void Ver_usuarios(DataGrid dataGridView1)
        {
            if (cn.State.ToString() == "Closed")
            {
                cn.Open();
            }
            try
            {
                SqlDataAdapter adap = new SqlDataAdapter("select Id_Usuario, Nombre, Nombre2, Paterno, Materno, Niv_Sec,Id_Honda from USUARIOS", cn);
                System.Data.DataSet ds = new System.Data.DataSet();
                adap.Fill(ds, "HONDA.USUARIOS");
                dataGridView1.DataSource = ds.Tables[0];
            }
            catch (Exception ex)
            {
                Exedy_Login.AutoClosingMessageBox.Show("No se conecto con la base de datos: " + ex.ToString(),"DB",3000);
            }
        }

        public int CrearCuentas(string Nombre, string Nombre2, String Paterno, String Materno, int id_Honda, int Niv_sec, string contraseña)
        {
            if (cn.State.ToString() == "Closed")
            {
                cn.Open();
            }
            DateTime time = DateTime.Now;              // Use current time
            SqlCommand Comando = new SqlCommand(string.Format("Insert Into USUARIOS (Nombre, Nombre2, Paterno, Materno, Id_Honda, Niv_Sec, Contraseña) values ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}' )", Nombre, Nombre2, Paterno, Materno, id_Honda, Niv_sec, contraseña), cn);
            Comando.ExecuteNonQuery();
            Comando.Dispose();
            //cn.Dispose();
            SqlConnection.ClearAllPools();
            return 0;
        }
    }
}
