using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HONDA
{
    public partial class Registro : MaterialSkin.Controls.MaterialForm
    {
        SqlConnection con;
        SqlDataAdapter adap;
        DataSet ds;
        public Registro(int id)
        {
            InitializeComponent();
            this.id = id;
        }
        int id;
        public static string Data;
        private void Registro_Load(object sender, EventArgs e)
        {
            MaterialSkin.MaterialSkinManager skinManager = MaterialSkin.MaterialSkinManager.Instance;
            skinManager.AddFormToManage(this);
            skinManager.Theme = MaterialSkin.MaterialSkinManager.Themes.LIGHT;
            skinManager.ColorScheme = new MaterialSkin.ColorScheme(MaterialSkin.Primary.Blue400, MaterialSkin.Primary.BlueGrey900, MaterialSkin.Primary.BlueGrey500, MaterialSkin.Accent.Green700, MaterialSkin.TextShade.WHITE);
            try
            {
                con = new SqlConnection() { };
                con.ConnectionString = @"Data Source=192.168.0.100;Initial Catalog=HONDA;Persist Security Info=True;User ID=Admin;Password=megadeth";
                con.Open();

                adap = new SqlDataAdapter("select Id_Usuario, Nombre, Nombre2, Paterno, Materno, Niv_Sec,Id_Honda from USUARIOS", con);
                //dataGridView1.Columns[0].Visible = false;
                ds = new System.Data.DataSet();
                adap.Fill(ds, "HONDA.USUARIOS");
                dataGridView1.DataSource = ds.Tables[0];
            }
            catch (Exception ex)
            {
                Exedy_Login.AutoClosingMessageBox.Show("No se conecto con la base de datos: " + ex.ToString(),"",3000);
            }
            con.Close();

        }

        private void Button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            try
            {
                String conec = "Data Source=192.168.0.100;Initial Catalog=HONDA;Persist Security Info=True;User ID=Admin;Password=megadeth";
                SqlConnection cnnn = new SqlConnection(conec);
                cnnn.Open();
                int Id_Usuario = 1;
                string query = "insert into usuarios (Nombre, Nombre2, Paterno, Materno, Id_Honda, Niv_Sec, Contraseña )  VALUES (@param1, @param2, @param3,  @param4, @param5, @param6, @param7)";

                using (SqlCommand cmd = new SqlCommand(query, cnnn))
                {
                    cmd.CommandTimeout = 1200000;
                    cmd.Parameters.AddWithValue("@param1", textBox1.Text);
                    cmd.Parameters.AddWithValue("@param2", textBox2.Text);
                    cmd.Parameters.AddWithValue("@param3", textBox3.Text);
                    cmd.Parameters.AddWithValue("@param4", textBox4.Text);
                    cmd.Parameters.AddWithValue("@param5", Convert.ToInt32(textBox5.Text));
                    cmd.Parameters.AddWithValue("@param6", Convert.ToInt32(textBox6.Text));
                    cmd.Parameters.AddWithValue("@param7", textBox7.Text);
                    Id_Usuario = Convert.ToInt32(cmd.ExecuteScalar());
                    cmd.Connection.Close();
                    con.Close();


                    try
                    {
                        con = new SqlConnection() { };
                        con.ConnectionString = @"Data Source=192.168.0.100;Initial Catalog=HONDA;Persist Security Info=True;User ID=Admin;Password=megadeth";
                        con.Open();
                        adap = new SqlDataAdapter("select Id_Usuario, Nombre, Nombre2, Paterno, Materno, Niv_Sec,Id_Honda from USUARIOS", con);
                        //dataGridView1.Columns[0].Visible = false;
                        ds = new System.Data.DataSet();
                        adap.Fill(ds, "HONDA.USUARIOS");
                        dataGridView1.DataSource = ds.Tables[0];
                        con.Close();
                    }
                    catch (Exception ex)
                    {
                        Exedy_Login.AutoClosingMessageBox.Show("No se conecto con la base de datos: " + ex.ToString(),"BD",3000);
                    }
                }
                Exedy_Login.AutoClosingMessageBox.Show("Agregado exitoso","",3000);
                textBox1.Text = "";
                textBox2.Text = "";
                textBox3.Text = "";
                textBox4.Text = "";
                textBox5.Text = "";
                textBox6.Text = "";
                textBox7.Text = "";
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
        }

        private void Eliminar(int id)
        {
            try
            {
                int i;
                i = dataGridView1.CurrentRow.Index;
                i++;
                Exedy_Login.AutoClosingMessageBox.Show("Ususario eliminado","",3000);
                con = new SqlConnection() { };
                con.ConnectionString = @"Data Source=192.168.0.100;Initial Catalog=HONDA;Persist Security Info=True;User ID=Admin;Password=megadeth";
                con.Open();
                adap = new SqlDataAdapter("delete USUARIOS where id_Usuario =" + id, con);
                //dataGridView1.Columns[0].Visible = false;
                ds = new System.Data.DataSet();
                adap.Fill(ds, "HONDA.USUARIOS");
                dataGridView1.DataSource = ds.Tables[0];
                con.Close();

                try
                {
                    con = new SqlConnection() { };
                    con.ConnectionString = @"Data Source=192.168.0.100;Initial Catalog=HONDA;Persist Security Info=True;User ID=Admin;Password=megadeth";
                    con.Open();
                    adap = new SqlDataAdapter("select Id_Usuario, Nombre, Nombre2, Paterno, Materno, Niv_Sec,Id_Honda from USUARIOS", con);
                    //dataGridView1.Columns[0].Visible = false;
                    ds = new System.Data.DataSet();
                    adap.Fill(ds, "HONDA.USUARIOS");
                    dataGridView1.DataSource = ds.Tables[0];
                    con.Close();
                }
                catch (Exception ex)
                {
                    Exedy_Login.AutoClosingMessageBox.Show("No se conecto con la base de datos: " + ex.ToString(),"",3000);
                }

            }
            catch (Exception ex)
            {
                Exedy_Login.AutoClosingMessageBox.Show("No se conecto con la base de datos: " + ex.ToString(),"",3000);
            }
            Usuarios();
        }

        private void TextBox8_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                int id = Convert.ToInt32(textBox8.Text);
                Eliminar(id);
                Usuarios();
                textBox8.Text = "";
            }
        }

        private void Usuarios()
        {
            try
            {
                con = new SqlConnection() { };
                con.ConnectionString = @"Data Source=192.168.0.100;Initial Catalog=HONDA;Persist Security Info=True;User ID=Admin;Password=megadeth";
                con.Open();
                adap = new SqlDataAdapter("select Id_Usuario, Nombre, Nombre2, Paterno, Materno, Niv_Sec,Id_Honda from USUARIOS", con);
                //dataGridView1.Columns[0].Visible = false;
                ds = new System.Data.DataSet();
                adap.Fill(ds, "HONDA.USUARIOS");
                dataGridView1.DataSource = ds.Tables[0];
                con.Close();
            }
            catch (Exception ex)
            {
                Exedy_Login.AutoClosingMessageBox.Show("No se conecto con la base de datos: " + ex.ToString(),"",3000);
            }
        }
    }
}
